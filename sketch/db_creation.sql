CREATE DATABASE cookweb;

-- -------------*ACCOUNT+LOGIN ATTEMPS*---------------  

CREATE TABLE `user` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
  `username` VARCHAR(30) NOT NULL, 
  `email` VARCHAR(50) NOT NULL, 
  `password` CHAR(128) NOT NULL, 
  `salt` CHAR(128) NOT NULL,
  `type` CHAR(128) NOT NULL
) ENGINE = InnoDB;

ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE `login_attempts` (
  `user_id` INT(11) NOT NULL,
  `time` VARCHAR(30) NOT NULL 
) ENGINE=InnoDB;


-- -------------*COURSES*---------------         

CREATE TABLE `course` (
  `id` int(11) NOT NULL,
  `title` char(50) NOT NULL,
  `image` VARCHAR(100) NOT NULL,
  `idCategory` int(11) NOT NULL,
  `chef` char(50) NOT NULL,
  `place` char(50) NOT NULL,
  `price` int(11) NOT NULL,
  `totTickets` int(11) NOT NULL,
  `ticketsLeft` int(11),
  `description` TEXT NOT NULL,
  `numLessons` int(11) NOT NULL,
  `date` date NOT NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `idOrganiser` int(11) NOT NULL
    
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

ALTER TABLE `course`
ADD PRIMARY KEY (`id`),
ADD FOREIGN KEY (`idOrganiser`) REFERENCES user(`id`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `course`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;



-- -------------*LESSONS*---------------         

CREATE TABLE `lesson` (
  `id` int(11) NOT NULL,
  `idCourse` int(11) NOT NULL,
  `title` char(50) NOT NULL,
  `description` TEXT NOT NULL,
  `date` date NOT NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

ALTER TABLE `lesson`
ADD PRIMARY KEY (`id`, `idCourse`),
ADD FOREIGN KEY (`idCourse`) REFERENCES course(`id`) ON UPDATE CASCADE ON DELETE CASCADE;


-- -------------*EVENT*---------------         

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `title` char(50) NOT NULL,
  `idChef` char(50) NOT NULL,
  `date` date NOT NULL,
  `idTimeSlot` char(50) NOT NULL,
  `time` time NOT NULL,
  `price` int(11),
  `totPlaces` int(11),
  `ticketsLeft` int(11)
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

ALTER TABLE `event`
ADD PRIMARY KEY (`id`);

ALTER TABLE `event`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;



-- -------------*TIME-SLOT*---------------         

CREATE TABLE `timeSlot` (
  `id` int(11) NOT NULL,
  `name` char(50) NOT NULL    
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

ALTER TABLE `timeSlot`
ADD PRIMARY KEY (`id`);

ALTER TABLE `timeSlot`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;



-- -------------*BANNER*---------------  

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `name` char(50) NOT NULL,
  `title` char(50) NOT NULL,
  `img` char(50) NOT NULL,
  `idCourse` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

ALTER TABLE `banner`
ADD PRIMARY KEY (`id`),
ADD FOREIGN KEY (`idCourse`) REFERENCES course(`id`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `banner`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;



-- -------------*CATEGORIES*---------------  

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` char(50) NOT NULL,
  `title` char(50) NOT NULL,
  `img` char(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

ALTER TABLE `category`
ADD PRIMARY KEY (`id`);

ALTER TABLE `category`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;



-- -------------*CART ITEMS*--------------- 
CREATE TABLE `cartItem`(
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`idUser` INT NOT NULL,
	`idCourse` int(11) NOT NULL,
	`quantity` int(11) DEFAULT 1
) ENGINE=InnoDB;

ALTER TABLE `cartItem`
ADD FOREIGN KEY (`idUser`) REFERENCES user(`id`) ON UPDATE CASCADE ON DELETE CASCADE,
ADD FOREIGN KEY (`idCourse`) REFERENCES course(`id`) ON UPDATE CASCADE ON DELETE CASCADE;



-- -------------*SUBSCRIPTION*--------------- 
CREATE TABLE `subscription`(
	`idUser` INT NOT NULL,
	`idCourse` int(11) NOT NULL,
	`quantity` int(11) NOT NULL,
  primary key (idUser, idCourse)
) ENGINE=InnoDB;

ALTER TABLE `subscription`
ADD FOREIGN KEY (`idUser`) REFERENCES user(`id`) ON UPDATE CASCADE ON DELETE CASCADE,
ADD FOREIGN KEY (`idCourse`) REFERENCES course(`id`) ON UPDATE CASCADE ON DELETE CASCADE;

-- -----------*NOTIFICATION*--------------------
CREATE TABLE `notification`(
	`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`type` char(50) NOT NULL,
	`idUser` int(11) NOT NULL,
	`idCourse` int(11) NOT NULL,
	`titleCourse` char(50) NOT NULL
) ENGINE=InnoDB;

-- -------------*CARD*--------------- 
CREATE TABLE `card`(
	`id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `numCard` CHAR(128) NOT NULL,
  `owner` char(50) NOT NULL,
	`idUser` int(11) NOT NULL,
  `month` int NOT NULL,
  `year` int NOT NULL
) ENGINE=InnoDB;

ALTER TABLE `card`
ADD FOREIGN KEY (`idUser`) REFERENCES user(`id`) ON UPDATE CASCADE ON DELETE CASCADE


