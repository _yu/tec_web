<?php

function isActive($pagename){
    if(basename($_SERVER['PHP_SELF'])==$pagename){
        echo " class='active' ";
    }
}

function isUserLoggedIn(){
    return !empty($_SESSION['user_id']);
}

function redirect($url) {
    header('Location: '.$url);
    exit();
}

?>