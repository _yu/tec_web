<?php
require_once 'database/dbNotification.php';
require_once 'database/dbLogin.php';
require_once 'database/dbAccount.php';

if(isset($_SESSION['user_id'])){
  $userType = getAccountType($dbh->getDb());
  if($userType == "organiser"){ 
    $templateParams["notifications"] = getOrganiserNotifs($dbh->getDb(), $_SESSION['user_id']);
  } else if($userType == "normal"){      
    $templateParams["notifications"] = getNormalNotifs($dbh->getDb(), $_SESSION['user_id']);
  }
  if(empty($templateParams["notifications"])){      
    $templateParams["notifications"] = "none";
  }
} else {      
  $templateParams["notifications"] = "none";
}

?>