<?php
require_once 'bootstrap.php';
require_once 'database/dbCart.php';

$templateParams["title"] = "C4L - Cart";
$templateParams["filename"] = "cart.php";

if(isset($_SESSION['user_id'])){
  $templateParams["cart-item"] = getCartItems($dbh->getDb(), $_SESSION['user_id']);
} else {
  $templateParams["cart-item"] = "notLoggedIn";
}

if(isset($_POST["cartItemId"], $_POST["cartItemQuantity"])) {
  setItemQuantity($dbh->getDb(), $_POST["cartItemQuantity"], $_POST["cartItemId"]);
}

if(isset($_POST["remove_cartItemId"])){
  removeCartItem($dbh->getDb(), $_POST["remove_cartItemId"]);
}

require 'template/template.php';
?>