<?php

require_once 'bootstrap.php';
require_once 'database/dbLogin.php';

if(isset($_POST["email"], $_POST["p"])) { 
    $dbResult = login($_POST["email"], $_POST["p"], $dbh->getDb());
    if($dbResult == SUCCESS) {
        redirect("./index.php");
    } else if ($dbResult == FAIL || $dbResult == NONEXISTENT) {
        $templateParams["login_error"] = "Indirizzo e-mail e/o password errati. Per favore, controlla e riprova";
    } else if ($dbResult == TOO_MANY_ATTEMPS) {
        $templateParams["login_error"] = "Account temporaneamente bloccato. Contatta gli admin per maggiori informazioni";
    }
}

if (!isUserLoggedIn()){
    $templateParams["title"] = "C4L - Login";
    $templateParams["filename"] = "login-form.php";
}

require 'template/template.php';
?>

