
$(document).ready(function(){

    const name = document.getElementById("name");
    const email = document.getElementById("email");
    const password = document.getElementById("password");
    const v_password = document.getElementById("v-password");
        
    const name_error = document.getElementById("name_error");
    const email_error = document.getElementById("email_error");
    const pwd_error = document.getElementById("password_error");

    $(".edit-info").click(function() {
        let type = $(this).attr("id");
        if (type == "nameBtn") {
            $("#nameModal").css("display", "block");
        } else if (type == "emailBtn") {
            $("#emailModal").css("display", "block");
        } else if (type == "passwordBtn") {
            $("#passwordModal").css("display", "block");
        }
    });

    $("input[type=submit]").click(function(e){
        e.preventDefault();
        if ($(this).attr("id") == "nameSubmit") {
            if (verifyName(name, name_error)) {
                sendName($("#name").val()); 
            }
        } else if ($(this).attr("id") == "emailSubmit") {
            if (verifyEmail(email, email_error)) {
                sendEmail($("#email").val()); 
            }
        } else if ($(this).attr("id") == "passwordSubmit") {
            if (verifyPassword(password, v_password, pwd_error)) {
                formhash($("#changePwd"), $("#password"));
                sendPassword($("#pId").val()); 
            }
        }
    });

    $(".close").click(function(){
        $(".modal").css("dispaly", "none");
    })

    window.onclick = function(event) {
        if ($(event.target).attr('class') == "modal") {
            $(".modal").each(function(){
                $(this).css("display", "none");
            });
        }
    }
});

function sendName(name) {
    $.ajax({
        url:"./modify-info-ajax.php",
        data: "newName="+name,
        type : 'post',
        success:function(data){
            alert("Il salvataggio del tuo nuovo nome ha avuto successo!");
            $("#nameModal").css("display", "none");
            $("#nameInfo").html(data);
        }
    });
}

function sendEmail(email) {
    $.ajax({
        url:"./modify-info-ajax.php",
        data: "newEmail="+email,
        type : 'post',
        success:function(data){
            if (data == "fail") {
                alert("L'e-mail che hai inserito è già associato ad un account. Controlla e riprova");
            } else {
                alert("Il salvataggio della tua nuova e-mail ha avuto successo!");
                $("#emailModal").css("display", "none");
                $("#emailInfo").html(data);
            }
        }
    });
}

function sendPassword(password) {
    $.ajax({
        url:"./modify-info-ajax.php",
        data: "newPassword="+password,
        type : 'post',
        success:function(data){
            alert("Il salvataggio della tua nuova password ha avuto successo!");
            $("#passwordModal").css("display", "none");
        }
    });
}
