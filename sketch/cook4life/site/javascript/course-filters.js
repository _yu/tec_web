$(document).ready(function() {

    $(document).on('dblclick','.className',function(){
        if(this.checked){
            $(this).prop('checked', false);
        }
    });

    if($(window).width() > 900) {
        $(".accordion:first").addClass("active");
        $(".panel:first").show();
        $(".panel:first").addClass("p_active"); 
        $("tabs li:first").addClass("active")           
    }     

    $(".accordion").click(function() {
        if ($(this).attr("class") == "accordion active") {
            clearAll();
        } else {
            clearAll();
            $(this).addClass("active");
            $(this).next("div").addClass("p_active");
            const tab = $(this).next("div").attr("id");
            addClassToTabs(tab);
        }
    });

    $("ul.tabs li").click(function() {
        $(".panel").removeClass("p_active");
        $(".accordion-container button").removeClass("active");
        $("ul.tabs li").removeClass("active");
        let activeTab = $(this).attr("rel"); 
        let activeButton = $("#"+activeTab).prev();
        $("#"+activeTab).show();
        $("#"+activeTab).addClass("p_active");  
        $(this).addClass("active");
        activeButton.addClass("active");
    });


    $("#filter-btn").click(function() {
        let filters = document.getElementById("filter-container");
        if($(window).width() > 900) {
            filters.style.height = "fit-content";
            $(".accordion:first").addClass("active");
            $(".panel:first").show();
            $(".panel:first").addClass("p_active"); 
            $("tabs li:first").addClass("active");  
        }
        $("#container").addClass("hide");
        $("#filter-container").addClass("show");
    });

    $("#go-back").click(function() {
        $("#container").removeClass("hide");
        $("#filter-container").removeClass("show");
    });

    $("#apply").click(function() {
        uncheckAll();
    });
});

function clearAll() {
    $("ul.tabs li").removeClass("active");
    $(".accordion").removeClass("active");
    $(".panel").removeClass("p_active");
}

function addClassToTabs(rel) {
    $("ul.tabs li").each(function() {
        if ($(this).attr("rel") == rel) {
            $(this).addClass("active");
        }
    });
}

