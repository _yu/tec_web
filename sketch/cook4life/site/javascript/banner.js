$(document).ready(function(){
  var index = 0;  
  $(".slide").hide();
  $(".slide").eq(0).show();
  $(".dot").eq(0).css("background-color", "#707070");

  $("#right-arrow").click(function(){
    $(".slide").eq(index).hide();
    $(".dot").eq(index).css("background-color", " #a8a8a8");
    index = (index + 1)%($(".slide").length);
    $(".slide").eq(index).show();
    $(".dot").eq(index).css("background-color", "#707070");
  });

  $("#left-arrow").click(function(){
    $(".slide").eq(index).hide();
    $(".dot").eq(index).css("background-color", " #a8a8a8");
    index = (index - 1)%($(".slide").length);
    $(".slide").eq(index).show();
    $(".dot").eq(index).css("background-color", "#707070");
  });

  $(".dot").click(function(){
    index = $(".dot").index($(this));
    for(let i = 0; i < $(".dot").length; i++){
      /* find which dot is currently selected */
      if($(".dot").eq(i).css("background-color") != "rgb(187, 187, 187)"){
        let target = $(".dot").index($(".dot").eq(i));
        $(".slide").eq(target).hide();
        $(".dot").eq(target).css("background-color", " #a8a8a8");
      }
    }
    $(".slide").eq(index).show();
    $(".dot").eq(index).css("background-color", "#707070");
  });

});
