$( document ).ajaxComplete(function() {
    $(".delete-course").click(function() {
        let numCourse = $(this).attr("id");
        let courseDiv = "#container"+numCourse;
        $(courseDiv).remove();
        $.ajax({
            url:"./account-courses.php",
            data: "delete-course="+numCourse,
            type : 'post'
        });
    });
});