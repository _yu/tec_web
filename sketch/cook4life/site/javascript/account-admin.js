$(document).ready(function(){
  /* remove course */
  $("#confirm-remove-course").click(function(){
    $idCourse = $("#remove-course").val();
    $titleCourse = $("#remove-course option:selected").text();
    $("#remove-course").val('');
    $("option[value="+$idCourse+"]").hide();
    $.ajax({
      url:"./account.php",
      data: "remove_courseId="+$idCourse,
      type: 'post'
    });
    $("#log-remove-course").removeClass("hidden");
    $("#log-remove-course").text("Hai rimosso il corso \""+$titleCourse+"\"");
  });

  $("#confirm-remove-normal").click(function(){
    $idUser = $("#remove-normal").val();
    $username = $("#remove-normal option:selected").text();
    $("#remove-normal").val('');
    postUserId($idUser);
    $("#log-remove-normal").removeClass("hidden");
    $("#log-remove-normal").text("Hai rimosso l'utente \""+$username+"\"");
  });
  
  $("#confirm-remove-organiser").click(function(){
    $idUser = $("#remove-organiser").val();
    $username = $("#remove-organiser option:selected").text();
    $("#remove-organiser").val('');
    postUserId($idUser);
    $("#log-remove-organiser").removeClass("hidden");
    $("#log-remove-organiser").text("Hai rimosso l'organizzatore \""+$username+"\"");
  });

  function postUserId($idUser){
    $("option[value="+$idUser+"]").hide();
    $.ajax({
      url:"./account.php",
      data: "remove_userId="+$idUser,
      type: 'post'
    });
  }
})