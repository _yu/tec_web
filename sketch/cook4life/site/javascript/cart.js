$(document).ready(function(){

  $(".item").each(function(){
    computeItemTotal($( this ));
  });
  computeCartTotal();
  checkIfEmpty();

  function computeItemTotal(item){
    var num = item.find(".num").text();
    var price = item.children("#price").val();
    item.find(".total-placeholder").text(num*price);
  }

  function computeCartTotal(){
    var tot = 0;
    $(".total-placeholder").each(function(){
      tot = tot + parseInt($(this).text());
    });

    $("#cart-total p").text("Il totale del tuo carrello è " + tot + "€");
  }
  
  $(".add").click(function(event){
    var btn = $(event.target);
    var num = btn.siblings(".num").text();
    var item =  btn.parents(".item");
    /* check that qty doesnt exceed num places left */
    if(parseInt(num) < parseInt(item.children("#ticketsLeft").val())){
      num++;
      btn.siblings(".num").text(num);
      var itemId = item.children("#itemId").val();
      computeItemTotal(item);
      updateQty(itemId, num);
    }
    
  });

  $(".sub").click(function(event){
    var btn = $(event.target);
    var num = btn.siblings(".num").text();
    if(num > 1){ 
      num--;
      btn.siblings(".num").text(num);
      var item =  btn.parents(".item");
      var itemId = item.children("input:hidden").val();
      computeItemTotal(item);
      updateQty(itemId, num);
    }
  });

  function updateQty(itemId, quantity) {
    $.ajax({
      url : "./cart.php",
      data : "cartItemId="+itemId+"&cartItemQuantity="+quantity,
      type : 'post'
    });
    computeCartTotal();
  }

  $(".item-remove button").click(function(event){
    var btn = $(event.target);
    var item =  btn.parents(".item");
    var itemId = item.children("#itemId").val();
    item.next().remove();
    item.remove();
    $.ajax({
      url : "./cart.php",
      data : "remove_cartItemId="+itemId,
      type : 'post'
    });
    
    computeCartTotal();
    checkIfEmpty();
  });

  function checkIfEmpty(){
    if(($(".item").length == 0)){
      $("#cart-total p").text("Il carrello è vuoto");
      $("#cart-total button").hide();
      if ($(window).width() < 600){ 
        $("#cart-total").css("padding", "50vw 10px");
        $("#cart-not-logged-in").css("padding", "50vw 10px");
      } else {
        $("#cart-total").css("padding", "11vw 10px");
        $("#cart-not-logged-in").css("padding", "8vw 10px");
      }
    } else {
        $("#cart-total").css("padding", "30px 10px");
    }
  }

  $(window).resize(function(){
    if ($(window).width() > 600){ 
      if(($(".item").length == 0)){
          $("#cart-total").css("padding", "11vw 10px");
          $("#cart-not-logged-in").css("padding", "8vw 10px");
      }
    } else {
      if(($(".item").length == 0)){
        $("#cart-total").css("padding", "50vw 10px");
        $("#cart-not-logged-in").css("padding", "50vw 10px");
    } 
    }
  });

})