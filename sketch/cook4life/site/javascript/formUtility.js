function formhash(form, password) {
    var p = document.createElement("input");
    form.append(p);
    p.id = "pId"
    p.name = "p";
    p.type = "hidden";
    p.value = hex_sha512(password.val());
    password.val("");
}

function verifyName(name, name_error) {
    if (name.value == "") {
        name.style.border = "1px solid red";
        name_error.textContent = "Inserisci il tuo nome";
        return false;
    } else {
        name.style.border = "1px solid grey";
        name_error.textContent = "";
        return true;
    }
}

function verifyEmail(email, email_error) {
    const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (email.value === "") {
        email.style.border = "1px solid red";
        email_error.textContent = "Inserisci un indirizzo e-mail";
        return false;
    } else if (!email.value.match(mailformat)) {
        email.style.border = "1px solid red";
        email_error.textContent = "Inserisci un indirizzo e-mail valido";
        return false;
    } else {
        email.style.border = "1px solid grey";
        email_error.textContent = "";
        return true;
    }
}

function verifyPassword(pwd, v_pwd, pwd_error) {
    if (pwd.value.length < 8) {
        pwd.style.border = "1px solid red";
        pwd_error.textContent = "La password deve avere almeno 8 caratteri";
        return false;
    } else if (pwd.value != v_pwd.value) {
        pwd.style.border = "1px solid red";
        pwd_error.textContent = "Le password non coincidono. Riprova";
        return false;
    } else {
        pwd.style.border = "1px solid grey";
        pwd_error.textContent = "";
        return true;
    }
}
