$(document).ready(function(){

    /*ACCOUNT-COURSES */
    $.ajax({
        url:"./account-courses-ajax.php",
        data: "none="+1,
        type: 'post',
        success:function(data){
            $('#user-courses').html(data);
        }
    });

    $.ajax({
        url:"./account-courses-ajax.php",
        data: "noneO="+1,
        type: 'post',
        success:function(data){
            $('#organiser-courses').html(data);
        }
    });
    
    $("#all-courses").click(function(e) {
        getNew = 0;
        getOld = 0;
        if ($(this).attr("class") == "organiser") {
            $.ajax({
                url:"./account-courses-ajax.php",
                data: "noneO="+1,
                type: 'post',
                success:function(data){
                    $('#organiser-courses').html(data);
                }
            });
        } else {
            $.ajax({
                url:"./account-courses-ajax.php",
                data: "none="+1,
                type: 'post',
                success:function(data){
                    $('#user-courses').html(data);
                }
            });
        }
    })

    $("#current-courses").click(function(e) {
        if ($(this).attr("class") == "organiser") {
            $.ajax({
                url:"./account-courses-ajax.php",
                data: "getNewO="+1,
                type : 'post',
                success:function(data){
                    $('#organiser-courses').html(data);
                }
            });
        } else {
            $.ajax({
                url:"./account-courses-ajax.php",
                data: "getNew="+1,
                type : 'post',
                success:function(data){
                    $('#user-courses').html(data);
                }
            });
        }
    })

    $("#old-courses").click(function(e) {
        if ($(this).attr("class") == "organiser") {
            $.ajax({
                url:"./account-courses-ajax.php",
                data: "getOldO="+1,
                type : 'post',
                success:function(data){
                    $('#organiser-courses').html(data);
                }
            });
        } else {
            $.ajax({
                url:"./account-courses-ajax.php",
                data: "getOld="+1,
                type : 'post',
                success:function(data){
                    $('#user-courses').html(data);
                }
            });
        }
    })
});


