$(document).ready(function() {

    $("form input").on("input selectionchange propertychange",function(e){
        $(this).val(e.target.value);
    });

    $("form input[type=\"submit\"]").click(function(e) {
        e.preventDefault();
        
        $("form input").each(function(){
            const value = $(this).val();
            if ($(this).attr("type") == "text") {
                if(value === "" || value === undefined){
                    $(this).css("border", "1px solid red");
                    $(this).next().remove();
                    $(this).after("<p>Campo obbligatorio</p>");
                } else {
                    $(this).css("border", "1px solid lightgrey");
                    if ($(this).next().prop("tagName") === "P" ) {
                        $(this).next().remove();
                        $(this).after("<br>");
                    };
                }
            }
        });

        const select =  $("#order-by");

        if (select.find(":selected").text() === "Categoria" ){
            select.css("border", "1px solid red");
            select.next().remove();
            select.after("<p>Seleziona una categoria</p>");
        } else {
            select.css("border", "1px solid lightgrey");
            if (select.next().prop("tagName") === "P" ) {
                select.next().remove();
                select.after("<br>");
            };
        }
        
    });
});