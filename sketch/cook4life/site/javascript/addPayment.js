$(document).ready(function() {
    $("#month").html(getMonth());
    $("#year").html(getYear());

    document.getElementById('card').addEventListener('keydown', function(e) {
        if (e.which === 38 || e.which === 40) {
            e.preventDefault();
        }
    });

    $("input[type=\"submit\"]").click(function(e){
        if($("#owner").val() != null && $("#owner").val() != undefined && 
        $("#card").val() != null && $("#card").val() != undefined) {
            $("form").submit();
        } else {
            e.preventDefault();
        }
    });

    $(".remove-card").click(function(e){
        let numCard = $(this).attr("id");
        let card = "#card"+numCard;
        $(card).remove();
        if ($("#your-payment").children().length == 1) {
            $("#your-payment").remove();
        }
        console.log($("#your-payment").children().length);
        $.ajax({
            url:"./account-payment.php",
            data: "delete="+numCard,
            type: 'post'
        });
    });
});

function getMonth() {
    let options = "";
    for(let i = 1; i <= 12; i++) {
        if (i==1) {
            options = options + "<option value=\"0"+i+"\" selected>"+"0"+i+"</option>";
        } else if (i<10) {
            options = options + "<option value=\"0"+i+"\">"+"0"+i+"</option>";
        } else {
            options = options + "<option value=\""+i+"\">"+i+"</option>";
        }
    }
    return options;
}

function getYear() {
    let options = "";
    for(let i = new Date().getFullYear(); i <= new Date().getFullYear() + 20; i++) {
        if (i == new Date().getFullYear()) {
            options = options + "<option value=\""+i+"\" selected>"+i+"</option>";
        } else {
            options = options + "<option value=\""+i+"\">"+i+"</option>";
        }
    }
    return options;
}