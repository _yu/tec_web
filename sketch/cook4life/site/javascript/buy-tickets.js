$(document).ready(function(){
  if($("#ticketsLeft").val() == 0){
    $("#buy").attr("disabled", "disabled");
    $("#buy").text("POSTI ESAURITI");
    $("#buy").css( "background-color", "#9c5a51");     
    $("#buy").css("cursor", "default");
  }

  computePrice();
  $("#get-course").click(function(e) {
      if($(window).width() > 900) {
          e.preventDefault();     
      } else {
          if ($("#buy-ticket section").attr("class") == "hide") {
              console.log($("#buy-ticket section").attr("class"));
              $("#buy-ticket section").removeClass("hide");
              $("#buy-ticket section").addClass("show");
              $("#buy-ticket div#arrow").css("display", "block");
          } else {
              $("#buy-ticket section").removeClass("show");
              $("#buy-ticket section").addClass("hide");
              $("#buy-ticket div#arrow").css("display", "none");
          }
      }
  });

  $("#buy").click(function(){
    if($("#userId").val() == "notLoggedIn"){
      $("#buy").text("esegui l'accesso prima di continuare");
      $("#buy").css( "background-color", "#9c5a51");     
      $("#buy").css("cursor", "default");
    } else {     
      $.ajax({
        url : "./course-details-ajax.php",
        data : "addToCart_courseId="+$("#courseId").val()+"&addToCart_quantity="+$(".num").text(),
        type : 'post',
        success: function (data) {
          applyChange(data);
        }
      });
    }
  });

  $(".add").click(function(){
    var num = $(".num").text();
    var item =  $("#courseId").val();
    /* check that qty doesnt exceed num places left */
    if(parseInt(num) < parseInt($("#ticketsLeft").val())){
      num++;
      $(".num").text(num);
      computePrice();
    }
  });

  $(".sub").click(function(event){
    var num = $(".num").text();
    var item =  $("#courseId").val();
    /* check that qty doesnt exceed num places left */
    if(num > 1){
      num--;
      $(".num").text(num);
      computePrice();
    }
  });

  $(window).scroll(() => { 
    if($(window).width() > 600) {
      // Distance from top of document to top of footer.
      topOfFooter = $('footer').position().top;
      // Distance user has scrolled from top, adjusted to take in height of sidebar (570 pixels inc. padding).
      scrollDistanceFromTopOfDoc = $(document).scrollTop() + 570;
      // Difference between the two.
      scrollDistanceFromTopOfFooter = scrollDistanceFromTopOfDoc - topOfFooter;
    
      // If user has scrolled further than footer,
      // pull sidebar up using a negative margin.
      if (scrollDistanceFromTopOfDoc > topOfFooter) {
          $('#buy-ticket').css('margin-top',  0 - scrollDistanceFromTopOfFooter);
      } else  {
          $('#buy-ticket').css('margin-top', "5%");
      }        
    }  
    });

    $(window).resize(function(){
      if ($(window).width() > 600){ 
        topOfFooter = $('footer').position().top;
      scrollDistanceFromTopOfDoc = $(document).scrollTop() + 570;
      scrollDistanceFromTopOfFooter = scrollDistanceFromTopOfDoc - topOfFooter;
      if (scrollDistanceFromTopOfDoc > topOfFooter) {
          $('#buy-ticket').css('margin-top',  0 - scrollDistanceFromTopOfFooter);
      } else  {
          $('#buy-ticket').css('margin-top', "5%");
      }        
    } else {
      $('#buy-ticket').css('margin-top', "0");
      }
  });

    function computePrice(){
      $price = $("#price").val();
      $quantity = $(".num").text();
      $price = $price*$quantity;
      $(".total-placeholder").text($price);
    }

    function applyChange(data) {
      if (data == "fail") {
        alert("Gli organizzatori non possono acquistare biglietti. Accedi con un account normale.");
      } else if (data == "done"){
        $("#buy").attr("disabled", "disabled");
        $("#buy").text("AGGIUNTO AL CARRELLO");
        $("#buy").css( "background-color", "#9c5a51");     
        $("#buy").css("cursor", "default");
        if($(window).width() < 900) {
          $("#buy-ticket section").removeClass("show");
          $("#buy-ticket section").addClass("hide");
          $("#buy-ticket div#arrow").css("display", "none");   
        }
      } else if (data == "ended") {
        alert("Non è più possibile acquistare questo corso.");
      }
    }
});