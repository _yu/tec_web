$(document).ready(function(){
  /* TODO: if user not logged in, manage notif button */
  if($("#notifYN").val() == "Y"){
    turnNotifOn();
  }

  $("#menu-toggle-btn").click(function(){
    $("#toggle-menu-container").toggle();
    $("#toggle-notif-container").hide();
    $("#menu-toggle-btn").toggleClass("menu-icon-on");
    $("#menu-toggle-btn").toggleClass("menu-icon-off");
  });

  function turnNotifOn(){
    $("#notif-toggle-btn").addClass("notif-icon-on");
    $("#notif-toggle-btn").removeClass("notif-icon-off");
  }

  function turnNotifOff(){
    $("#notif-toggle-btn").addClass("notif-icon-off");
    $("#notif-toggle-btn").removeClass("notif-icon-on");
  }

  function toggleNotif(){
    $("#notif-toggle-btn").toggleClass("notif-icon-on");
    $("#notif-toggle-btn").toggleClass("notif-icon-off");
  }

  $(".notif").click(function(event){
    $li = $(event.target);
    $idNotif = $li.siblings("input:hidden").val();
    $li.hide();
    $.ajax({
      url : "./bootstrap.php",
      data : "idNotification="+$idNotif,
      type : 'post'
    });
  })

  $("#notif-toggle-btn").click(function(){
    if($("#notifYN").val() == "Y"){
      toggleNotif();
    }
    $("#toggle-notif-container").toggle();
    $("#toggle-menu-container").hide();
  })
})