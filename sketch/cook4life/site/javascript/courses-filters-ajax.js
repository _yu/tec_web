var filter = {
    category: 0,
    duration: 0,
    price: 0,
    time: 0,
    order: 0
};

$(document).ready(function(){

    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
    
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
    
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    };

    if (getUrlParameter == null || getUrlParameter == undefined) {

        $.ajax({
            url:"./course-ajax.php",
            type : 'post',
            dataType: 'json',
            success:function(data){
                $('#courses-display').html(data[0]);
                $("#count-courses").html(data[1]);
            }
        });
    } else {
        let category = getUrlParameter('category');
        
        if (category == "pastery") {
            filter["category"] = "pastery";
            $("#pastery").prop('checked', true);
        } else if (category == "italian-cuisine") {
            filter["category"] = "italian-cuisine";
            $("#italian-cuisine").prop('checked', true);
        } else if (category == "international-cuisine") {
            filter["category"] = "international-cuisine";
            $("#international-cuisine").prop('checked', true);
        } else if (category == "tasting") {
            filter["category"] = "tasting";
            $("#tasting").prop('checked', true);
        }

        $.ajax({
            url:"./course-ajax.php",
            data: {category: filter.category, duration: filter.duration,
                price: filter.price, time: filter.time, order: filter.order},
            type : 'post',
            dataType: 'json',
            success:function(data){
                $('#courses-display').html(data[0]);
                $("#count-courses").html(data[1]);
            }
        });
    }
    
    $("#apply").click(function() {
        uncheckAll();
        $.ajax({
            url:"./course-ajax.php",
            type : 'post',
            dataType: 'json',
            success:function(data){
                $('#courses-display').html(data[0]);
                $("#count-courses").html(data[1]);
            }
        });
    });

    $(".panel").on("input selectionchange propertychange", function(){
        const filter_name = $(this).find("input").attr("name");
        getFilter(filter_name);

        $.ajax({
            url:"./course-ajax.php",
            data: {category: filter.category, duration: filter.duration,
                price: filter.price, time: filter.time, order: filter.order},
            type : 'post',
            dataType: 'json',
            success:function(data){
                $('#courses-display').html(data[0]);
                $("#count-courses").html(data[1]);
            }
        });
    });

    $("#order-by").bind("change", function(){
        filter.order = $(this).val();
        $.ajax({
            url:"./course-ajax.php",
            data: {category: filter.category, duration: filter.duration,
                price: filter.price, time: filter.time, order: filter.order},
            type : 'post',
            dataType: 'json',
            success:function(data){
                $('#courses-display').html(data[0]);
                $("#count-courses").html(data[1]);
            }
        });
    });
});

function getFilter(class_name) {
    const filter_value = $("input[name="+class_name+"]:checked").val();
    filter[class_name] = filter_value;
}

function uncheckAll() {
    $('input:radio').each(function () { $(this).prop('checked', false); });
}



