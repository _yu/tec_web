$(document).ready(function(){
    
// Distance from top of document to top of footer.
topOfFooter = $('footer').position().top;
    
if (topOfFooter < 400) {
    $('.container').css('padding',  "25px 4% 50vw");
    $('#filter-container').css('padding-bottom',  "50vw");
} else  {
    $('.container').css('padding',  "25px 4%");
    $('#filter-container').css('padding-bottom',  "0");
}      
      
$(window).resize(function(){
    if ($(window).width() < 600){ 
      if (topOfFooter < 400) {
        $('.container').css('padding',  "25px 4% 50vw");
        $('#filter-container').css('padding-bottom',  "50vw");
      } 
    } else {
        $('#filter-container').css('padding-bottom',  "0");
        $('.container').css('padding',  "25px 4%");
    }
});
    
});