function signUpValidation() {
    const name = document.registration["name"];
    const email = document.registration["email"];
    const password = document.registration["password"];
    const v_password = document.registration["v-password"];
        
    const name_error = document.getElementById("name_error");
    const email_error = document.getElementById("email_error");
    const pwd_error = document.getElementById("password_error");

    if (verifyName(name, name_error) && verifyEmail(email, email_error)
        && verifyPassword(password, v_password, pwd_error)) {
        formhash($("form"), $("#password"));
        return true;
    } else {
        return false;
    }
}
