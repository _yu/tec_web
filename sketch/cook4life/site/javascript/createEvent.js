$(document).ready(function() {

    document.getElementById('price').addEventListener('keydown', function(e) {
        if (e.which === 38 || e.which === 40) {
            e.preventDefault();
        }
    });

    document.getElementById('totTickets').addEventListener('keydown', function(e) {
        if (e.which === 38 || e.which === 40) {
            e.preventDefault();
        }
    });

    document.getElementById('date').addEventListener('keydown', function(e) {
        if (e.which === 38 || e.which === 40) {
            e.preventDefault();
        }
    });

    document.getElementById('startTime').addEventListener('keydown', function(e) {
        if (e.which === 38 || e.which === 40) {
            e.preventDefault();
        }
    });

    document.getElementById('endTime').addEventListener('keydown', function(e) {
        if (e.which === 38 || e.which === 40) {
            e.preventDefault();
        }
    });

    let currentTab = 0;
    let numLessons = 0;
    showTab(currentTab);

    /* ADD LIMITS TO DATE */
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth()+1; //January is 0!
    let yyyy = today.getFullYear();
    if(dd < 10){
        dd = '0' + dd;
    } 
    if(mm < 10){
        mm = '0' + mm;
    }
    min = yyyy + '-' + mm + '-' + dd;
    max =  (yyyy+2)+ '-' + mm + '-' + dd;
    
    $("input[type=date]").attr("min", min);
    $("input[type=date]").attr("max", max);

    $("#prevBtn").click(function(e) {
        e.preventDefault();
        let x = document.getElementsByClassName("tab");
        x[currentTab].style.display = "none";
        currentTab = currentTab - 1;
        showTab(currentTab);
    });

    $("#nextBtn").click(function(e) {
        e.preventDefault();
        let x = document.getElementsByClassName("tab");
        if (!validateForm()) {
            return false;
        }
        x[currentTab].style.display = "none";
        currentTab = currentTab + 1;

        if (currentTab == 2) {
            /*document.cookie="numLessons="+$("#numLessons").val();*/
            let i = 0;
            if (numLessons != $("#numLessons").val()) {
                let text = "<h2>Compila i seguenti campi</h2>";
                numLessons = $("#numLessons").val();
                for (i=1; i <= numLessons; i++) {
                    text = text +
                        "<label for=\"LessonTitle"+i+"\">Titolo della lezione "+i+"</label>"+
                        "<input type=\"text\" name=\"LessonTitle"+i+"\" id=\"lessonTitle"+i+"\" placeholder=\"Descrizione\"/><br>"+
                        "<label for=\"LessonDescription"+i+"\">Breve descrizione della lezione "+i+"</label><br>"+
                        "<textarea name=\"LessonDescription"+i+"\" id=\"lessonDescription"+i+"\"rows=\"5\" cols=\"40\"></textarea><br>";
                }
                x[currentTab].innerHTML = text;
            }
        } else if (currentTab == 3) {
            let array = [];
            let text = "<br>";
            $("input").not("input[type=submit]").each(function() {
                array.push($(this));
            });
            let index = 1;
            let p = x[currentTab].getElementsByTagName("p");
            for (i = 0; i <= 11; i++) {
                if (i == 1) {
                    p[i].innerHTML = "Immagine del corso: " + "<br>";
                    index = index + 1;
                } else if (i==2) {
                    p[i].innerHTML = "Categoria: " + $("#category option:selected").text();
                } else if (i==7) {
                    p[i].innerHTML = "Descrizione del corso:" +  $("#courseDescription").val();
                } else {
                    p[i].innerHTML = array[index].attr("name") + ": " + array[index].val();
                    index = index + 1;
                }
            }
            let lesson = 1;
            for (i = 11; i < array.length; i++) {
                text = text + "<p>Titolo lezione " + lesson + ": " + array[i].val() + "</p>"; 
                text = text + "<p>Descrizione lezione " + lesson + ": </p>"+ "<p>"+ $("#lessonDescription"+lesson).val(); + "</p>";
                lesson = lesson + 1;
            }
            
            x[currentTab].getElementsByClassName("lessons")[0].innerHTML = text;
        }
        
        if (currentTab >= x.length) {
            document.getElementById("course-form").submit();
            return false;
        }
        showTab(currentTab);
    });

    function showTab(n) {
        var x = document.getElementsByClassName("tab");
        x[n].style.display = "block";

        if (n == 0) {
            document.getElementById("prevBtn").style.display = "none";
        } else {
            document.getElementById("prevBtn").style.display = "inline";
        }

        if (n == (x.length - 1)) {
            $("#nextBtn").attr("value", "Invia");
        } else {
            $("#nextBtn").attr("value", "Avanti");
        }

        fixStepIndicator(n)
    }

    function validateForm() {
        var x, y, i, valid = true;
        x = document.getElementsByClassName("tab");
        y = x[currentTab].getElementsByTagName("input");
        z = x[currentTab].getElementsByTagName("textarea");
        const select =  $("#category");

        if (select.find(":selected").text() === "Categoria" ){
            select.addClass("invalid");
            valid = false;
        } else {
            select.removeClass("invalid");
        }

        for (i = 0; i < y.length; i++) {
            if (y[i].value == "") {
                y[i].className += " invalid";
                valid = false;
            } else {
                y[i].className -= " invalid";
            }
        }

        
        for (i = 0; i < z.length; i++) {
            if (z[i].value == "") {
                z[i].className += " invalid";
                valid = false;
            } else {
                z[i].className -= " invalid";
            }
        }

        if ($("#endTime").val() != undefined && $("#startTime").val() != undefined &&
            $("#endTime").val() != null && $("#startTime").val() != null) {
            const startTime = $("#startTime").val();
            const endTime = $("#endTime").val();
            const st = new Date('00','00','00',startTime.split(':')[0],startTime.split(':')[1], 00);
            const et = new Date('00','00','00',endTime.split(':')[0],endTime.split(':')[1], 00);
            if (et.getTime() < st.getTime()) {
                valid = false;
                $("#endTime").addClass("invalid");
            }
        }

        if (valid) {
            document.getElementsByClassName("step")[currentTab].className += " finish";
        }
        return valid;
    }

    function fixStepIndicator(n) {
        var i, x = document.getElementsByClassName("step");
        for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", "");
        }
        x[n].className += " active";
    }

    window.onload = function() {

        var fileInput = document.getElementById('fileInput');
        var fileDisplayArea = document.getElementById('fileDisplayArea');

        fileInput.addEventListener('change', function(e) {
            var file = fileInput.files[0];
            var imageType = /image.*/;

            if (file.type.match(imageType)) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    fileDisplayArea.innerHTML = "";

                    var img = new Image();
                    img.src = reader.result;
                    img.width = 100;
                    img.height = 100;
                    fileDisplayArea.appendChild(img);
                }

                reader.readAsDataURL(file); 
            } else {
                fileDisplayArea.innerHTML = "File not supported!"
            }
        });

}
});