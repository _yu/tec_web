<?php

require_once 'bootstrap.php';
require_once 'database/dbCourse.php';

$query = "SELECT Co.id as id, CO.title as title, CA.title as category, image, price, ticketsLeft,
date, startTime, endTime
FROM course CO, category CA
WHERE CO.idCategory = CA.id
AND CO.id IN (SELECT C1.id
            FROM lesson l, course C1
            WHERE C1.id = CO.id
            AND l.idCourse = C1.id
            HAVING min(l.date) > (SELECT CURRENT_DATE)
            )";

if (isset($_POST["category"])) {
    $category = $_POST["category"];
    if ($category == "pastery") {
        $query .= " AND CA.id = 1 ";
    } else if ($category == "italian-cuisine") {
        $query .= " AND CA.id = 2 ";
    } else if ($category == "international-cuisine") {
        $query .= " AND CA.id = 3 ";
    } else if ($category == "tasting") {
        $query .= " AND CA.id = 4 ";
    }
}

if(isset($_POST["duration"])) {
    $duration = $_POST["duration"];
    if ($duration == "one-three") {
        $query .= " AND (CO.numLessons >= 1 AND CO.numLessons <= 3) ";
    } else if ($duration == "four-five") {
        $query .= " AND (CO.numLessons >= 4 AND CO.numLessons <= 5) ";
    } else if ($duration == "five-ten") {
        $query .= " AND (CO.numLessons >= 5 AND CO.numLessons <= 10) ";
    } else if ($duration == "more-than-ten") {
        $query .= " AND CO.numLessons >= 10 ";
    }
}

if(isset($_POST["price"])) {
    $price = $_POST["price"];
    if ($price == "zero-onehundred") {
        $query .= " AND (CO.price >= 0 AND CO.price <= 100) ";
    } else if ($price == "one-hundred-two-hundred") {
        $query .= " AND (CO.price >= 100 AND CO.price <= 200) ";
    } else if ($price == "two-hundred-three-hundred") {
        $query .= " AND (CO.price >= 200 AND CO.price <= 300) ";
    } else if ($price == "three-hundred-four-hundred") {
        $query .= " AND (CO.price >= 300 AND CO.price <= 400)  ";
    } else if ($price == "four-hundred-more") {
        $query .= " AND CO.price >= 400 ";
    }
}

if(isset($_POST["time"])) {
    $time = $_POST["time"];
    if ($time == "morning") {
        $query .= " AND CO.endTime < \"14:00:00\" ";
    } else if ($time == "afternoon") {
        $query .= " AND CO.startTime >  \"12:00:00\" AND CO.endTime < \"19:00:00\" ";
    } else if ($time == "evening") {
        $query .= " AND CO.startTime >=  \"18:00:00\" ";
    } else if ($time == "weekend") {
        $query .= " AND CO.id IN (SELECT C2.id
                                FROM course C2
                                WHERE C2.id = Co.id
                                AND (DAYNAME(C2.date) = \"Sunday\" OR DAYNAME(C2.date) = \"Saturday\")) ";
    }
}

if(isset($_POST["order"])) {
    $order = $_POST["order"];
    if ($order == "date") {
        $query .= " ORDER BY CO.date ASC";
    } else if ($order == "shortest") {
        $query .= " ORDER BY CO.numLessons ASC";
    } else if ($order == "longest") {
        $query .= " ORDER BY CO.numLessons DESC ";
    } else if ($order == "least-expensive") {
        $query .= " ORDER BY CO.price ASC ";
    } else  if ($order == "most-expensive") {
        $query .= " ORDER BY CO.price DESC ";
    }
}

$stmt = ($dbh->getDb())->prepare($query);
$stmt->execute();
$result = $stmt->get_result();
$result = $result->fetch_all(MYSQLI_ASSOC);

$output = "";
$num = count($result);

if (!empty($result)) {
    foreach($result as $course){
        $output .= "
            <section id=course".$course["id"]." class=\"course\">
            <p><span class=\"course-date\">".$course["date"]."<br>".$course["startTime"]."-".$course["endTime"]."</span></p>
            <img class=\"course-img\" src=\"./site/img/courses/".$course["image"]."\" alt=\"\"/>
            <p class=\"type-of-course\">".$course["category"]."</p>
            <p class=\"course-name\">".$course["title"]."</p>
            <p class=\"price\">Prezzo: €".$course["price"]."</p>
            <p class=\"tickets-left\">Posti disponibili: <span class=\"num-tickets-left\">".$course["ticketsLeft"]."</span></p>
            <a href=\"course-details.php?idevent=".$course["id"]."\">
                <button class=\"see-details\">VEDI DETTAGLI</button>
            </a>
        </section>";	
    }
} else {
    $output .= "<p id=\"not-found\">Nessun risultato trovato</p>";
}

$array = array($output, $num);
echo json_encode($array);

?>