<?php

function insertNewUser($db, $name, $email, $p, $type) {
    $password = $p; 
    $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true)); //create a casual key
    $password = hash('sha512', $password.$random_salt); //create password with salt
    $numAcc = checkEmail($db, $email);
    echo $numAcc;

    if ($numAcc == 0) {
        $stmt = $db->prepare("INSERT INTO user (username, email, password, salt, type) VALUES (?, ?, ?, ?, ?)");   
        $stmt->bind_param('sssss', $name, $email, $password, $random_salt, $type); 
        $stmt->execute();
        return true;
    } else {
        return false;
    }
}

function checkEmail($db, $email) {
    $stmt = $db->prepare("SELECT COUNT(id) FROM user WHERE email = ?");
    $stmt->bind_param('s', $email);
    $stmt->execute();
    $result = $stmt->get_result();
    $array = $result->fetch_all(MYSQLI_ASSOC);
    $tmp = $array[0];
    return $tmp["COUNT(id)"];
}
?>