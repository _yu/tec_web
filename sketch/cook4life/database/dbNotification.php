<?php
require_once 'database/dbAccount.php';

  function getOrganiserNotifs($db, $idOrganiser){
    $query = "SELECT * FROM notification WHERE idUser=?";
    $stmt = $db->prepare($query);
    $stmt->bind_param('i', $idOrganiser);
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_all(MYSQLI_ASSOC);
  }

  function getNormalNotifs($db, $idUser){
    $query = "SELECT * FROM notification WHERE idUser=?";
    $stmt = $db->prepare($query);
    $stmt->bind_param('i', $idUser);
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_all(MYSQLI_ASSOC);
  }
  
  function deleteNotification($db, $id){
    $stmt = $db->prepare("DELETE FROM notification WHERE id = ?");
    $stmt->bind_param('i', $id);
    $stmt->execute();
  }

?>