<?php

define("HOST", "localhost"); //server name
define("USER", "root"); //database user name
define("PASSWORD", ""); //password
define("DATABASE", "cookweb"); //db name


class DatabaseHelper{
    private $db;

    public function __construct(){
        $this->db = new mysqli(HOST, USER, PASSWORD, DATABASE);
        if ($this->db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }        
    }

    public function getDb() {
        return $this->db;
    }

    /*call this function at the start of every page that needs a session variable*/
    function sec_session_start() {
        $session_name = 'sec_session_id'; //create session name variable
        $secure = false; //set to true when using "https"
        $httponly = true; //stop scripts to access the session' id
        ini_set('session.use_only_cookies', 1); //force session to only use cookie
        $cookieParams = session_get_cookie_params(); //read cookies' current parametrs
        session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly); 
        session_name($session_name); //set session_name with the chosen name
        session_start(); //start php session
        session_regenerate_id(); //regenerate session id and delete previous session id
    }
}
?>