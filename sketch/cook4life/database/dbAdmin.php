<?php
  function getAllCourses($db){
    $stmt = $db->prepare("SELECT * FROM course");
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_all(MYSQLI_ASSOC);
  }

  function getCourseTitle($db, $id){
    $stmt = $db->prepare("SELECT title FROM course WHERE id=?");
    $stmt->bind_param('i', $id);
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_all(MYSQLI_ASSOC)[0]["title"];
  }

  function getInterestedUsers($db, $idCourse){
    $stmt = $db->prepare("SELECT idUser FROM cartitem WHERE idCourse=? UNION SELECT idUser FROM subscription WHERE idCourse=?");
    $stmt->bind_param('ii', $idCourse, $idCourse);
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_all(MYSQLI_ASSOC);
  }

  function removeCourse($db, $idCourse){
    /* add notifications */
    $courseTitle = getCourseTitle($db, $idCourse);
    $interestedUsers = getInterestedUsers($db, $idCourse);
    foreach($interestedUsers as $user){
      $stmt1 = $db->prepare("INSERT INTO notification (id, type, idUser, idCourse, titleCourse) VALUES (DEFAULT, 'deleted', ?, ?, ?)");
      $stmt1->bind_param('iis', $user["idUser"], $idCourse, $courseTitle);
      $stmt1->execute();
    }  

    $stmt = $db->prepare("DELETE FROM course WHERE id = ?");
    $stmt->bind_param('i', $idCourse);
    $stmt->execute();
  }

  function getAllNormalUsers($db){
    $stmt = $db->prepare("SELECT * FROM user WHERE type = 'normal'");
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_all(MYSQLI_ASSOC);
  }

  function getAllOrganisers($db){
    $stmt = $db->prepare("SELECT * FROM user WHERE type = 'organiser'");
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_all(MYSQLI_ASSOC);
  }

  function removeUser($db, $idUser){
    $stmt = $db->prepare("DELETE FROM user WHERE id = ?");
    $stmt->bind_param('i', $idUser);
    $stmt->execute();
  }
?>