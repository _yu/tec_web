<?php

function addCourse($courseArray, $lessonArray, $file, $db){
    if (login_check($db)) {
        $stmt = $db->prepare("INSERT INTO `course`
        (`title`, `idCategory`, `chef`, `place`, `price`, `totTickets`,
        `description`, `date`, `numLessons`, `startTime`, `endTime`, `idOrganiser`, `ticketsLeft`, `image`) 
        VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

        $courseArray[7] = DateTime::createFromFormat("Y-m-d", $courseArray[7])->format("Y-m-d");
        $courseArray[9] = DateTime::createFromFormat("H:i",$courseArray[9])->format("H:i");
        $courseArray[10] = DateTime::createFromFormat("H:i",$courseArray[10])->format("H:i");
        $image = $file["name"];
        $params = $courseArray;
        array_push($params, $courseArray["5"]);
        array_push($params, $image);
        $target = "./site/img/courses/".basename($image);
        foreach ($params as $a){
            echo $a."    ";
        }

        $stmt->bind_param("sissiississiis", ...$params);
        if ($stmt->execute()) {
            move_uploaded_file($file['tmp_name'], $target);
            if (addLesson($lessonArray, $courseArray[7], $courseArray[9], $courseArray[10], $db)) {
                return SUCCESS;
            }
        } else {
            return ERROR;
        }
    } else {
        return ERROR;
    }
}

function addLesson($lessonArray, $courseDate, $startTime, $endTime, $db) {
    if (login_check($db)) {
        $index = 1;
        $courseId = getLastCourseId($db);
        $delta = 0;
        $time = 7;
        foreach($lessonArray as $lesson) {
            $stmt = $db->prepare("INSERT INTO `lesson`
            (`id`, `idCourse`, `title`, `description`, `date`, `startTime`, `endTime`) 
            VALUE (?, ?, ?, ?, ?, ?, ?)");

            $params = array();
            $params[0] = $index;
            $params[1] = $courseId;
            foreach($lesson as $info) {
                array_push($params, $info);
            }
            $jump = $delta*$time;
            $date = date('Y-m-d', strtotime($courseDate .' +'.$jump.' day'));
            array_push($params, $date);
            array_push($params, $startTime);
            array_push($params, $endTime);
            $stmt->bind_param("iisssss", ...$params);
            if ($stmt->execute()) {
                $delta++;
                $index++;
            } else {
                return ERROR;
            }
        }
        return SUCCESS;
    } else {
        return ERROR;
    }
}

function getLastCourseId($db) {
    $stmt = $db->prepare("SELECT max(id) FROM course");
    $stmt->execute();
    $result = $stmt->get_result();
    $array = $result->fetch_all(MYSQLI_ASSOC);
    $tmp = $array[0];
    return $tmp["max(id)"];
}
?>