<?php

function insertNewCard($db, $owner, $c, $month, $year) {
    if (checkCard($db, $c)) {
        $user_id = $_SESSION['user_id'];
        $stmt = $db->prepare("INSERT INTO card (numCard, owner, idUser, month, year) VALUES (?, ?, ?, ?, ?)");   
        $stmt->bind_param('isiii', $c, $owner, $user_id, $month, $year); 
        $stmt->execute();
        return true;
    } else {
        return false;
    }
}

function checkCard($db, $c) {
    $user_id = $_SESSION['user_id'];
    $stmt = $db->prepare("SELECT numCard FROM card");
    $stmt->execute();
    $result = $stmt->get_result();
    $array = $result->fetch_all(MYSQLI_ASSOC);

    if (!empty($array)) {
        foreach($array as $card) {
            if ($c == $card["numCard"]) {
                return false;
            }
        }
        return true;
    } else {
        return true;
    }
}

function getPaymentNormal($db) {
    $user_id = $_SESSION['user_id'];
    $stmt = $db->prepare("SELECT id, numCard, owner, month, year FROM card WHERE idUser = ?");
    $stmt->bind_param("i", $user_id);
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_all(MYSQLI_ASSOC);
}

function deleteCard($db, $id) {
    $stmt = $db->prepare(" DELETE FROM card WHERE id=? ");
    $stmt->bind_param('i', $id);
    $stmt->execute();
}
?>