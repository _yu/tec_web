<?php

define("TOO_MANY_ATTEMPS", 0);
define("SUCCESS", 1);
define("FAIL", 2);
define("NONEXISTENT", 3);

function login($email, $password, $db) {
    if ($stmt = $db->prepare("SELECT id, username, password, salt FROM user WHERE email = ? LIMIT 1")) { 
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($user_id, $username, $db_password, $salt); //save query results into variables
        $stmt->fetch();
        $password = hash('sha512', $password.$salt); //use univoc key to code password
        if($stmt->num_rows == 1) {
            if(checkbrute($user_id, $db) == true) { 
                    return TOO_MANY_ATTEMPS;
                } else {
                    if($db_password == $password) {          
                        $user_browser = $_SERVER['HTTP_USER_AGENT']; //get user-agent from current user
                        $user_id = preg_replace("/[^0-9]+/", "", $user_id); //prevent XSS attack
                        $_SESSION['user_id'] = $user_id; 
                        $username = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $username); //prevent XSS attack
                        $_SESSION['username'] = $username;
                        $_SESSION['login_string'] = hash('sha512', $password.$user_browser);
                        return SUCCESS;    
                    } else {
                        $now = time();
                        $db->query("INSERT INTO login_attempts (user_id, time) VALUES ('$user_id', '$now')");
                        return FAIL;
                    }
                }
        } else {
            return NONEXISTENT;
        }
    }
}


    function login_check($db) {
        // Verifica che tutte le variabili di sessione siano impostate correttamente
        if(isset($_SESSION['user_id'], $_SESSION['username'], $_SESSION['login_string'])) {
            $user_id = $_SESSION['user_id'];
            $login_string = $_SESSION['login_string'];
            $username = $_SESSION['username'];     
            $user_browser = $_SERVER['HTTP_USER_AGENT'];

            if ($stmt = $db->prepare("SELECT password FROM user WHERE id = ? LIMIT 1")) { 
                $stmt->bind_param('i', $user_id);
                $stmt->execute();
                $stmt->store_result();
        
                if($stmt->num_rows == 1) {
                    $stmt->bind_result($password);
                    $stmt->fetch();
                    $login_check = hash('sha512', $password.$user_browser);
                    if($login_check == $login_string) {
                        //login executed
                        return true;
                    } else {
                        //login NOT executed
                        return false;
                    }
                } else {
                    //user not found, login NOT executed
                    return false;
                }
            } else {
                //error with query, login NOT executed
                return false;
            }
        } else {
            //wrong variables, login NOT executed
            return false;
        }
    }

function checkbrute($user_id, $db) {
    $now = time(); //show last 2 hours attempts
    $valid_attempts = $now - (2*60*60); 
    if ($stmt = $db->prepare("SELECT time FROM login_attempts WHERE user_id = ? AND time > '$valid_attempts'")) { 
        $stmt->bind_param('i', $user_id); 
        $stmt->execute();
        $stmt->store_result();
        if($stmt->num_rows > 5) {
            return true;
        } else {
            return false;
        }
    }
    }
?>