<?php

function getBanner($db) {
    $stmt = $db->prepare("SELECT 'name', title, img, idCourse FROM banner");
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_all(MYSQLI_ASSOC);
}

function getCategory($db) {
    $stmt = $db->prepare("SELECT id, name, title, img FROM category");
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_all(MYSQLI_ASSOC);
}

?>