<?php

define("ERROR", 0);

function getAccountType($db) {
    if (login_check($db)) {
        $user_id = $_SESSION['user_id'];
        $stmt = $db->prepare("SELECT type FROM user WHERE id = ?");
        $stmt->bind_param('i', $user_id);
        $stmt->execute();
        $result = $stmt->get_result();
        $array = $result->fetch_all(MYSQLI_ASSOC);
        $tmp = $array[0];
        return $tmp["type"];
    } else {
        return ERROR;
    }
}

/*returns an associative array of the user info -> [username => xxxx, email => xxxx]*/
function getInfo($db) {
    $user_id = $_SESSION['user_id'];
    $stmt = $db->prepare("SELECT username, email FROM user WHERE id = ?");
    $stmt->bind_param('i', $user_id);
    $stmt->execute();
    $result = $stmt->get_result();
    $array = $result->fetch_all(MYSQLI_ASSOC);
    $tmp = $array[0];
    return $tmp;
}

function getBoughtCoursesNormal($db) {
    $user_id = $_SESSION['user_id'];
    $stmt = $db->prepare("SELECT DISTINCT S.idCourse as id, CO.title as title
                        FROM subscription S, course CO
                        WHERE S.idUser = ?
                        AND CO.id = S.idCourse");
    $stmt->bind_param('i', $user_id);
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_all(MYSQLI_ASSOC);
}

function getAllCoursesOrganiser($db) {
    $user_id = $_SESSION['user_id'];
    $stmt = $db->prepare("SELECT DISTINCT id, title
                        FROM course
                        WHERE idOrganiser = ?");
    $stmt->bind_param('i', $user_id);
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_all(MYSQLI_ASSOC);
}

function getCurrentCoursesNormal($db) {
    $user_id = $_SESSION['user_id'];
    $stmt = $db->prepare("SELECT DISTINCT S.idCourse as id, CO.title as title
                        FROM subscription S, course CO
                        WHERE S.idUser = ?
                        AND CO.id = S.idCourse
                        AND S.idCourse IN (SELECT C1.id
                                        FROM lesson l, course C1
                                        WHERE C1.id = CO.id
                                        AND l.idCourse = C1.id
                                        HAVING max(l.date) > (SELECT CURRENT_DATE)
                                        )");
    $stmt->bind_param('i', $user_id);
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_all(MYSQLI_ASSOC);                                   
}


function getCurrentCoursesOrganiser($db) {
    $user_id = $_SESSION['user_id'];
    $stmt = $db->prepare("SELECT DISTINCT id, title
                        FROM course CO
                        WHERE CO.idOrganiser = ?
                        AND CO.id IN (SELECT C1.id
                                    FROM lesson l, course C1
                                    WHERE C1.id = CO.id
                                    AND l.idCourse = C1.id
                                    HAVING max(l.date) > (SELECT CURRENT_DATE)
                                    )");
    $stmt->bind_param('i', $user_id);
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_all(MYSQLI_ASSOC);                                   
}

function getOldCoursesNormal($db) {
    $user_id = $_SESSION['user_id'];
    $stmt = $db->prepare("SELECT DISTINCT S.idCourse as id, CO.title as title
                        FROM subscription S, course CO
                        WHERE S.idUser = ?
                        AND CO.id = S.idCourse
                        AND S.idCourse IN (SELECT C1.id
                                        FROM lesson l, course C1
                                        WHERE C1.id = CO.id
                                        AND l.idCourse = C1.id
                                        HAVING max(l.date) < (SELECT CURRENT_DATE)
                                        )");
    $stmt->bind_param('i', $user_id);
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_all(MYSQLI_ASSOC);                                   
}

function getOldCoursesOrganiser($db) {
    $user_id = $_SESSION['user_id'];
    $stmt = $db->prepare("SELECT DISTINCT id, title
                        FROM course CO
                        WHERE CO.idOrganiser = ?
                        AND CO.id IN (SELECT C1.id
                                    FROM lesson l, course C1
                                    WHERE C1.id = CO.id
                                    AND l.idCourse = C1.id
                                    HAVING max(l.date) < (SELECT CURRENT_DATE)
                                    )");
    $stmt->bind_param('i', $user_id);
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_all(MYSQLI_ASSOC);                                   
}

function setNewName($db, $name) {
    $user_id = $_SESSION['user_id'];
    $stmt = $db->prepare("UPDATE user SET username = ? WHERE id = ?");
    $stmt->bind_param('si', $name, $user_id);
    $stmt->execute();
}

function setNewEmail($db, $email) {
    if (checkEmail($db, $email) == 0) {
        $user_id = $_SESSION['user_id'];
        $stmt = $db->prepare("UPDATE user SET email = ? WHERE id = ?");
        $stmt->bind_param('si', $email, $user_id);
        $stmt->execute();
        return true;
    } else {
        return false;
    }
}

function setNewPassword($db, $password) {
    $user_id = $_SESSION['user_id'];
    $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true)); //create a casual key
    $password = hash('sha512', $password.$random_salt); //create password with salt
    $stmt = $db->prepare("UPDATE user SET password = ?, salt = ? WHERE id = ?");
    $stmt->bind_param('ssi', $password, $random_salt, $user_id);
    $stmt->execute();
}

?>