<?php

/* return all cart items associated to user with $idUser */
function getCartItems($db, $idUser) {
    $stmt = $db->prepare("SELECT i.id, c.id as idCourse, c.title, c.chef, i.quantity, c.image, c.date, c.price, c.ticketsLeft, c.idOrganiser
                          FROM cartItem i, course c
                          WHERE i.idUser = ? AND i.idCourse = c.id");
    $stmt->bind_param('s', $idUser);
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_all(MYSQLI_ASSOC);
}

/* set cart quantity = $qty for cart item = $id*/
function setItemQuantity($db, $qty, $id){
  $stmt = $db->prepare("UPDATE cartItem SET quantity=? WHERE id=?");
  $stmt->bind_param('ii', $qty, $id);
  $stmt->execute();
}

/* remove cart item with id=$id*/
function removeCartItem($db, $id){
  $stmt = $db->prepare(" DELETE FROM cartItem WHERE id=? ");
  $stmt->bind_param('i', $id);
  $stmt->execute();
}

function getTicketsLeft($db, $id){
  $stmt = $db->prepare("SELECT ticketsLeft FROM course WHERE id = ?");
  $stmt->bind_param('i', $id);
  $stmt->execute();
  $result = $stmt->get_result();
  return $result->fetch_all(MYSQLI_ASSOC);
}

function getCourseTitle($db, $id){
  $stmt = $db->prepare("SELECT title FROM course WHERE id=?");
  $stmt->bind_param('i', $id);
  $stmt->execute();
  $result = $stmt->get_result();
  return $result->fetch_all(MYSQLI_ASSOC)[0]["title"];
}

function processPayment($db, $idUser){
  $items = getCartItems($db, $idUser);

  foreach($items as $item){
    /* decrease ticketsLeft in courses table */
    $idCourse = $item["idCourse"];
    $quantity = $item["quantity"];
    $ticketsLeft = getTicketsLeft($db, $idCourse)[0]["ticketsLeft"] - $quantity;
    if($ticketsLeft == 0){
      /* add notification */
      $courseTitle = getCourseTitle($db, $idCourse);
      $stmt0 = $db->prepare("INSERT INTO notification (id, type, idUser, idCourse, titleCourse) VALUES (DEFAULT, 'sold-out', ?, ?, ?)");
      $stmt0->bind_param('iis', $item["idOrganiser"], $idCourse, $courseTitle);
      $stmt0->execute();
      
    }
      $stmt1 = $db->prepare("UPDATE course SET ticketsLeft = ? WHERE id = ?");
      $stmt1->bind_param('ii', $ticketsLeft, $idCourse);
      $stmt1->execute();

      /* save ticket to user */
      $stmt2 = $db->prepare("INSERT INTO subscription (idUser, idCourse, quantity) VALUES (?, ?, ?)");
      $stmt2->bind_param('iii', $idUser, $item["idCourse"], $item["quantity"]);
      $stmt2->execute();

      /* delete cartItem */
      $stmt3 = $db->prepare("DELETE FROM cartItem WHERE id = ?");
      $stmt3->bind_param('i', $item["id"]);
      $stmt3->execute();

  }
}

function getCard($db) {
  $user_id = $_SESSION['user_id'];
  $stmt = $db->prepare("SELECT numCard FROM card WHERE idUser = ?");
  $stmt->bind_param("i", $user_id);
  $stmt->execute();
  $result = $stmt->get_result();
  return $result->fetch_all(MYSQLI_ASSOC);
}

?>
