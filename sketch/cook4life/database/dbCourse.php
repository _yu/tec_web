<?php

/*returns courses' informations to be displayed*/
function getAllCoursesInfo($db) {
    $stmt = $db->prepare("SELECT Co.id as id, CO.title as title, CA.title as category, image, price, ticketsLeft,
                            date, startTime, endTime
                        FROM course CO, category CA
                        WHERE CO.idCategory = CA.id
                        AND CO.id IN (SELECT C1.id
                                        FROM lesson l, course C1
                                        WHERE C1.id = Co.id
                                        AND l.idCourse = C1.id
                                        AND l.date > (SELECT CURRENT_DATE))");
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_all(MYSQLI_ASSOC);
}

function getNumCourses($db) {
    $stmt = $db->prepare("SELECT COUNT(*) FROM course");
    $stmt->execute();
    $result = $stmt->get_result();
    $tmp = $result[0];
    return $result->fetch_all(MYSQLI_ASSOC);
}

function getTicketsLeft($idEvent, $db) {
    $stmt = $db->prepare("SELECT ticketsLeft FROM course WHERE id=?");
    $stmt->bind_param("i", $idEvent);
    $stmt->execute();
    $result = $stmt->get_result();
    $array = $result->fetch_all(MYSQLI_ASSOC);
    $tmp = $array[0];
    return $tmp["ticketsLeft"];
}

function getTime($idEvent, $db) {
    $stmt = $db->prepare("SELECT startTime, endTime FROM course WHERE id=?");
    $stmt->bind_param("i", $idEvent);
    $stmt->execute();
    $result = $stmt->get_result();
    $array = $result->fetch_all(MYSQLI_ASSOC);
    $array[0]["startTime"] = DateTime::createFromFormat("H:i:s", $array[0]["startTime"])->format("H:i");
    $array[0]["endTime"] = DateTime::createFromFormat("H:i:s", $array[0]["endTime"])->format("H:i");
    return  $array;
}

function getGeneralInfo($idEvent, $db) {
    $stmt = $db->prepare("SELECT CO.title as title, CA.title as category, chef, place, numLessons, TIMEDIFF (endTime, startTime) as duration, price, description
                        FROM course CO, category CA
                        WHERE CO.idCategory = CA.id
                        AND Co.id=?");
    $stmt->bind_param("i", $idEvent);
    $stmt->execute();
    $result = $stmt->get_result();
    $array = $result->fetch_all(MYSQLI_ASSOC);
    $array[0]["duration"] = DateTime::createFromFormat("H:i:s", $array[0]["duration"])->format("G");
    $array[0]["description"] = nl2br($array[0]["description"]);
    return $array;
}

function getLessons($idEvent, $db) {
    $stmt = $db->prepare("SELECT L.title as title, L.description as description, L.date
                        FROM course C, lesson L
                        WHERE C.id=?
                        AND L.idCourse = ?");
    $stmt->bind_param("ii", $idEvent, $idEvent);
    $stmt->execute();
    $result = $stmt->get_result();
    $array = $result->fetch_all(MYSQLI_ASSOC);
    foreach ($array as $key => $field) {
        $array[$key]['description'] = nl2br($array[$key]['description']);
    }
    return $array;
}

function addToCart($db, $idUser, $idCourse, $quantity){
    /*query to select courses that haven't started yet*/
    $query = "SELECT id
            FROM course CO
            WHERE CO.id IN (SELECT C1.id
                        FROM lesson l, course C1
                        WHERE C1.id = CO.id
                        AND l.idCourse = C1.id
                        HAVING min(l.date) > (SELECT CURRENT_DATE)
                        )";
    $stmt1 = $db->prepare($query);
    $stmt1->execute();
    $result = $stmt1->get_result();
    $buyableCourses = array_column($result->fetch_all(MYSQLI_ASSOC), "id");

    if (in_array($idCourse, $buyableCourses)) {
        $stmt = $db->prepare("INSERT INTO `cartitem` VALUES(DEFAULT, ?, ?, ?)");
        $stmt->bind_param('iii', $idUser, $idCourse, $quantity);
        $stmt->execute();
        return true;
    } else {
        return false;
    }
}


function getInterestedUsers($db, $idCourse){
  $stmt = $db->prepare("SELECT idUser FROM cartitem WHERE idCourse=? UNION SELECT idUser FROM subscription WHERE idCourse=?");
  $stmt->bind_param('ii', $idCourse, $idCourse);
  $stmt->execute();
  $result = $stmt->get_result();
  return $result->fetch_all(MYSQLI_ASSOC);
}

function getCourseTitle($db, $id){
  $stmt = $db->prepare("SELECT title FROM course WHERE id=?");
  $stmt->bind_param('i', $id);
  $stmt->execute();
  $result = $stmt->get_result();
  return $result->fetch_all(MYSQLI_ASSOC)[0]["title"];
}

function removeCourse($db, $idCourse){
    /* add notification */
    $courseTitle = getCourseTitle($db, $idCourse);
    $interestedUsers = getInterestedUsers($db, $idCourse);
    foreach($interestedUsers as $user){
      $stmt1 = $db->prepare("INSERT INTO notification (id, type, idUser, idCourse, titleCourse) VALUES (DEFAULT, 'deleted', ?, ?, ?)");
      $stmt1->bind_param('iis', $user["idUser"], $idCourse, $courseTitle);
      $stmt1->execute();
    }  

    $stmt = $db->prepare(" DELETE FROM course WHERE id=? ");
    $stmt->bind_param('i', $idCourse);
    $stmt->execute();

}


?>