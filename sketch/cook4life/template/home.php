<header id="banner">
    <div id="banner-slideshow">
        <?php foreach($templateParams["banners"] as $banner): ?>
        <div class="slide">
            <img src=<?php echo BANNER_DIR.$banner["img"]; ?> alt="banner image" />
            <div class="banner-caption">
                <span><a href=<?php echo "course-details.php?idevent=".$banner["idCourse"]; ?>><?php echo $banner["title"]?></a></span>
            </div>
        </div>
        <?php endforeach; ?>
        
            
        <div class="arrows">
            <button id="left-arrow" class="banner-arrow btn-transparent"></button>
            <button id="right-arrow" class="banner-arrow btn-transparent"></button>
        </div>
    </div>
    <div class="dots">
        <?php foreach($templateParams["banners"] as $banner): ?>
            <span class="dot"></span>
        <?php endforeach; ?>
    </div>
</header>

<div class="home-display">
    <?php foreach($templateParams["categories"] as $category): ?>
	    <div id=<?php echo "category".$category["id"]; ?> class="category">
        <a href=<?php echo "course.php?category=".$category["name"]; ?> >
	    	<img class="category-img" src=<?php echo CATEGORY_DIR.$category["img"] ?> alt="category image" />
			  <span class="category-name"><?php echo $category["title"]; ?> </span>
        </a>
      </div>	
    <?php endforeach; ?>		
</div>


