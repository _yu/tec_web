<link rel="stylesheet" type="text/css" href="./site/css/account.css"/>
<script src="./site/javascript/account-courses-ajax.js"></script>
<script src="./site/javascript/fixFooter.js"></script>
<div class="container">
	
	<a class="go-back" href="account.php">Torna al mio account</a><br>
	<section id="summary">
        <h2>I tuoi corsi</h2>
        <button id="all-courses">Tutti i corsi</button>
        <button id="current-courses">Corsi attuali</button>
        <button id="old-courses">Corsi passati</button>
		<p id="user-courses">
		</p>
	</section>
</div>
