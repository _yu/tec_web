<link rel="stylesheet" type="text/css" href="./site/css/courses.css" />
<script src="./site/javascript/course-filters.js"></script>
<script src="./site/javascript/courses-filters-ajax.js"></script>
<script src="./site/javascript/fixFooter.js"></script>
<div id="filter-container">
	<section id="filters" class="clearfix">
		<button id="go-back">Chiudi</button>
		<p>Filtri</p>
		<button id="apply">Annulla filtri</button>
	</section>

	<div id="tab-container">
		<ul class="tabs">
			<li class="active" rel="tab1">CATEGORIA</li>
			<li rel="tab2">DURATA</li>
			<li rel="tab3">PREZZO</li>
			<li rel="tab4">FASCIA ORARIA</li>
		</ul>
	</div>

	<div class="accordion-container">
		<button class="accordion">CATEGORIA</button>
		<div id="tab1" class="panel">
			<input type="radio" name="category" id="pastery" value="pastery" class="className">
			<label for="pastery">Pasticceria</label>
					
			<input type="radio" name="category" id="italian-cuisine" value="italian-cuisine" class="className">
			<label for="italian-cuisine">Cucina Italiana</label>
					
			<input type="radio" name="category" id="international-cuisine" value="international-cuisine" class="className">
			<label for="international-cuisine">Cucina Internazionale</label>

			<input type="radio" name="category" id="tasting" value="tasting" class="className">
			<label for="tasting">Degustazione</label>
		</div>
				
		<button class="accordion">DURATA</button>
		<div id="tab2" class="panel">
            <input type="radio" name="duration" id="one-three" value="one-three" class="className">
            <label for="one-three">1 - 3 lezioni</label>
                    
            <input type="radio" name="duration" id="four-five" value="four-five" class="className">
            <label for="four-five">4 - 5 lezioni</label>
                        
            <input type="radio" name="duration" id="five-ten" value="five-ten" class="className">
            <label for="five-ten">5 - 10 lezioni</label>
                            
            <input type="radio" name="duration" id="more-than-ten" value="more-than-ten" class="className">
            <label for="more-than-ten">Più di 10 lezioni</label>
		</div>

		<button class="accordion">PREZZO</button>              
		<div id="tab3" class="panel">
			<input type="radio" name="price" id="zero-onehundred" value="zero-onehundred" class="className">
			<label for="zero-onehundred">€0 - €100</label>
						
			<input type="radio" name="price" id="one-hundred-two-hundred" value="one-hundred-two-hundred" class="className">
			<label for="one-hundred-two-hundred">€100 - €200</label>
						
			<input type="radio" name="price" id="two-hundred-three-hundred" value="two-hundred-three-hundred" class="className">
			<label for="two-hundred-three-hundred">€200 - €300</label>
									
			<input type="radio" name="price" id="three-hundred-four-hundred" value="three-hundred-four-hundred" class="className">
			<label for="three-hundred-four-hundred">€300 - €400</label>

			<input type="radio" name="price" id="four-hundred-more" value="four-hundred-more" class="className">
			<label for="four-hundred-more">Più di €400</label>
		</div>  

		<button class="accordion">FASCIA ORARIA</button>
		<div id="tab4" class="panel">
			<input type="radio" name="time" id="morning" value="morning" class="className">
			<label for="morning">Mattina</label>

			<input type="radio" name="time" id="afternoon" value="afternoon" class="className">
			<label for="afternoon">Pomeriggio</label>

			<input type="radio" name="time" id="evening" value="evening" class="className">
			<label for="evening">Sera</label>
						
			<input type="radio" name="time" id="weekend" value="weekend" class="className">
			<label for="weekend">Week-end</label>
		</div>    
	</div>
</div>

<div id="container">
	<section id="courses-navigation">
		<section id="button">
    		<button id="filter-btn">FILTRA CORSI</button>
        </section>
        <div class="clearfix">
		    <p><span id="count-courses"></span> CORSI</p>
			<select id="order-by">
				<option value="" disabled selected>Ordina per</option>
				<option value="date">Data</option>
				<option value="shortest">Meno lezioni</option>
				<option value="longest">Più lezioni</option>
				<option value="least-expensive">Prezzo più basso</option>
				<option value="most-expensive">Prezzo più alto</option>
			</select>
		</div>
	</section>

	<section id="header">
		<h2>I NOSTRI CORSI</h2>
    	<p>Impara a cucinare, ascoltando e degustando, perché il cibo, come il vino e la birra, ha delle storie
	        da raccontare. Corsi di cucina, di pasticceria, di degustazione, incontri speciali e grandi cene, entra
			e scopri tutte le proposte nella tua città, per imparare anche tu ad amare il cibo come noi.</p>
	</section>

	<div id="courses-display">				   
	</div>
</div>