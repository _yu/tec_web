<section id="signup">
    <script src="./site/javascript/sha512.js"></script>
    <script src="./site/javascript/formUtility.js"></script>
    <script src="./site/javascript/registration.js"></script>
    <link rel="stylesheet" type="text/css" href="./site/css/login-signup.css" />
    <?php if(isset($templateParams["signUp_error"])): ?>
        <p class="error-message"><?php echo $templateParams["signUp_error"]; ?></p>
	<?php endif; ?>
    
    <form action=# method="post" name="registration" onsubmit="return signUpValidation()">
        <h1>Crea il tuo account</h1>
        <label for="name">Nome</label>
        <input type="text" name="name" id="name"><br>
        <span id="name_error" class="error"></span>

        <label for="email">E-mail</label>
        <input type="text" name="email" id="email"><br>
        <span id="email_error" class="error"></span>

        <label for="password">Password</label>
        <input type="password" name="password" id="password" autocomplete="off">
        <p>Usa almeno 8 caratteri</p>
        <label for="v-password">Verifica password</label>
        <input type="password" name="v-password" id="v-password" autocomplete="off"><br>
        <span id="password_error" class="error"></span>
        <br>
        <p>Scegli che tipo di account vuoi fare:</p>
        <fieldset>
        <input type="radio" name="type" value="normal" id="normal" checked>
        <label class="type" for="normal">Cliente</label>
        <input type="radio" name="type" value="organiser" id="organiser">
        <label class="type" for="organiser">Organizzatore</label>
    </fieldset>

        <input type="submit" value="Registrati">
        <p>Registrandoti dichiari di aver letto e accetti integralmente le nostre Condizioni di utilizzo e<!-- 
        -->la nostra Informativa sulla privacy.</p>
        </form>
</section>