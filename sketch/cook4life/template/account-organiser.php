<link rel="stylesheet" type="text/css" href="./site/css/account.css"/>
<div class="container">
	<section id="summary">
		<h2>Riepilogo</h2>
		<p id="summary_content"><?php foreach($templateParams["summary"] as $info): ?>
				<?php echo $info;?><br>
			<?php endforeach; ?>
		</p>
		<div class="border"><!-- placeholder for border line --></div>
	</section>
	<section id="events-settings">
		<h2>I tuoi corsi</h2>
		<a href="account-courses.php">Visualizza o cancella i tuoi corsi</a><br>
		<a <?php isActive("create-course.php")?> href="create-course.php">Inserisci un nuovo corso</a><br>
		<div class="border"><!-- placeholder for border line --></div>
	</section>
	<section id="account-settings">
		<h2>Impostazioni</h2>
		<a href="modify-info.php">Modifica il tuo account</a><br>
		<a href="account-payment.php">Modifica il tuo metodo di pagamento</a>
	</section>
</div>