<script src="./site/javascript/payment.js"></script>
<div id="payment">

    <?php if($templateParams["cards"] == "noCards"): ?>
      <div id="cart-not-logged-in"> 
        <button type="submit" onclick="window.location.href = './account-payment.php';">Non hai inserito un metodo di pagamento: clicca qui per aggiungerlo</button>
      </div>
    <?php else: ?>
      <div>
      <label for="payment-method">Seleziona un metodo di pagamento:</label>
      <select id="payment-method">
        <?php foreach($templateParams["cards"] as $card): ?>
          <option value="card-option"><?php echo $card["numCard"]?></option>
        <?php endforeach; ?>
      </select> 
      </div>
      <button id="confirm-payment">Acquista</button>
      <p class="hidden">Pagamento completato!</p>
    <?php endif; ?>
</div>