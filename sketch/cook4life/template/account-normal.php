<link rel="stylesheet" type="text/css" href="./site/css/account.css"/>
<div class="container">
	<section id="summary">
		<h2>Riepilogo</h2>
		<p id="summary_content">
			<?php foreach($templateParams["summary"] as $info): ?>
				<?php echo $info?><br>
			<?php endforeach; ?>
		</p>
		<div class="border"><!-- placeholder for border line --></div>
	</section>
	

	<section id="subscribed-courses">
		<h2>Attualmente sei iscritto ai seguenti corsi:</h2>
		<p id="courses-content">
			<?php if (empty($templateParams["currentCourses"])):?>
			<?php echo "Non sei iscritto a nessun corso! Clicca <a href=\"course.php\">qui</a> per trovarne uno!<br>";?>
			<?php else:?>
				<?php foreach($templateParams["currentCourses"] as $course): ?>
				<?php echo "<a href=\"course-details.php?idevent=".$course["id"]."\">".$course["title"]."</a><br>";?>
				<?php endforeach; ?>
			<?php endif; ?>

		</p>
		<div class="border"><!-- placeholder for border line --></div>
	</section>

	<section id="events-settings">
		<h2>I tuoi corsi</h2>
		<a href="account-courses.php">Visualizza le tue iscrizioni</a><br>
		<div class="border"><!-- placeholder for border line --></div>
	</section>

	<section id="account-settings">
		<h2>Impostazioni</h2>
		<a <?php isActive("modify-info.php");?> href="modify-info.php">Modifica il tuo account</a><br>
		<a href="account-payment.php">Modifica il tuo metodo di pagamento</a>
	</section>
</div>
