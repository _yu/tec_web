<section id="login">
	<script src="./site/javascript/sha512.js"></script>
  <script src="./site/javascript/forms.js"></script>
  <link rel="stylesheet" type="text/css" href="./site/css/login-signup.css" />
	<?php if(isset($templateParams["login_error"])): ?>
        <p class="error-message"><?php echo $templateParams["login_error"]; ?></p>
	<?php endif; ?>
  
  <form method="post" name="login_form">
  <h1>Accedi</h1>
    <label for="email">E-mail</label>
    <input type="text" class="login-input" name="email" id="email" placeholder="E-mail" autocomplete="on"/><br>
    <label for="password">Password</label>
    <input type="password" class="login-input" name="password" id="password" placeholder="Password" autocomplete="on"/><br>
    <input type="submit" name="submit" value="Accedi" onclick="formhash(this.form, this.form.password);"/>

    <br><a href="#link8">Non riesci ad accedere?</a><br>
    <a href="registration.php">Non hai ancora un account?</a>
  </form>
</section>