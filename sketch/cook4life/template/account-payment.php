<link rel="stylesheet" type="text/css" href="./site/css/account.css"/>
<script src="./site/javascript/sha512.js"></script>
<script src="./site/javascript/formUtility.js"></script>
<script src="./site/javascript/addPayment.js"></script>
<script src="./site/javascript/fixFooter.js"></script>
<div class="container">
<br>
    <a class="go-back" href="account.php">Torna al mio account</a>

    <?php if(!empty($templateParams["card"])): ?>
        <section id="your-payment">
            <?php foreach($templateParams["card"] as $card): ?>
            <div class="card-container" id=<?php echo "card".$card["id"] ?>>
                <p class="user-card">
                    Numero carta: <?php echo $card["numCard"] ?><br>
                    Intestatario: <?php echo $card["owner"] ?><br>
                    <?php if($card["month"] < 10): ?>
                        Scadenza: <?php echo "0".$card["month"]."/".$card["year"] ?><br>
                    <?php endif ?>
                    <?php if($card["month"] >= 10): ?>
                        Scadenza: <?php echo $card["month"]."/".$card["year"] ?><br>
                    <?php endif ?>
                </p>
                <button class="remove-card" id=<?php echo $card["id"] ?>>Rimuovi</button>
            </div>
            <div class="border"><!-- placeholder for border line --></div>
            <?php endforeach; ?>
        </section>
    <?php endif ?>

	<section id="add-payment">

        <?php if(isset($templateParams["add_error"])): ?>
            <p class="error-message"><?php echo $templateParams["add_error"]; ?></p>
        <?php endif; ?>
        
        <form method="post" name="cart_form">
            <h1>Aggiungi un nuovo metodo di pagamento</h1>
            <p>Inserisci i dati della carta:</p><br>
                <label for="owner">Intestatario carta</label><br>
                <input type="text" name="owner" id="owner"/><br>
                <label for="card">Numero carta</label><br>
                <input type="number" min="0" name="card" id="card" oninput="validity.valid||(value='');"/><br>
                <p>Data di scadenza</p>
                <label for="month">Mese</label>
                <select id="month" name="month">
                </select>
                <label for="year">Anno</label>
                <select id="year" name="year">
                </select><br><br>
                <input type="submit" name="submit" value="Aggiungi"/>
            </form>
	</section>
</div>