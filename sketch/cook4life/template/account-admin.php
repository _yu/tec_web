<link rel="stylesheet" type="text/css" href="./site/css/account.css"/>
<script src="./site/javascript/account-admin.js"></script>
<div class="container">
	<section id="summary">
		<h2>Riepilogo</h2>
		<p id="summary_content"><?php foreach($templateParams["summary"] as $info): ?>
				<?php echo $info;?><br>
			<?php endforeach; ?>
		</p>
		<div class="border"><!-- placeholder for border line --></div>
	</section>
	<section id="manage-courses">
		<h2>Rimuovi corso</h2>
		<label for="remove-course">Seleziona un corso:</label>
    <select id="remove-course">
      <?php foreach($templateParams["courses"] as $course): ?>
        <option value="<?php echo $course["id"]?>"><?php echo $course["title"]?></option>
      <?php endforeach; ?>
    </select>
    <button id="confirm-remove-course">Rimuovi</button>
    <p class="hidden" id="log-remove-course">Hai rimosso il corso </p>
    
  <div class="border"><!-- placeholder for border line --></div>
  </section>
  

	<section id="manage-users">
		<h2>Rimuovi utente ordinario</h2>
		<label for="remove-normal">Seleziona un utente:</label>
    <select id="remove-normal">
      <?php foreach($templateParams["normals"] as $normal): ?>
        <option value="<?php echo $normal["id"]?>"><?php echo $normal["username"]?></option>
      <?php endforeach; ?>
    </select>
    <button id="confirm-remove-normal">Rimuovi</button>
    <p class="hidden" id="log-remove-normal"></p>


    <h2>Rimuovi utente organizzatore</h2>
		<label for="remove-organiser">Seleziona un utente:</label>
    <select id="remove-organiser">
      <?php foreach($templateParams["organisers"] as $organiser): ?>
        <option value="<?php echo $organiser["id"]?>"><?php echo $organiser["username"]?></option>
      <?php endforeach; ?>
    </select>
    <button id="confirm-remove-organiser">Rimuovi</button>
    <p class="hidden" id="log-remove-organiser"></p>
  </section>
  
</div>