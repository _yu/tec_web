<script src="./site/javascript/createEvent.js"></script>
<link rel="stylesheet" type="text/css" href="./site/css/account.css"/>
<div id="container">
    <br>
    <a class="go-back" href="account.php">Torna al mio account</a>
    <section id="course-creation">
        <form method="post" id="course-form" name="course-form" enctype="multipart/form-data">
            <h1>CREA UN NUOVO CORSO</h1><br>
            <div class="tab">
                <h2>Compila i seguenti campi</h2><br>
                <label for="title">Nome del corso</label>
                <input type="text" name="Nome del corso" id="title" placeholder="Nome"/><br>
                <label for="fileInput">Immagine del corso</label>
                <input type="file" name="image" id="fileInput"><br>
                <label for="category">Selezione la categoria del corso</label><br>
                <select id="category" name="idCategory">
                    <option value="" disabled selected>Categoria</option>
                    <option value="1">Pasticceria</option>
                    <option value="2">Cucina Italiana</option>
                    <option value="3">Cucina Estera</option>
                    <option value="4">Degustazione</option>
                </select><br>
                <label for="chef">Nominativo del chef</label>
                <input type="text" name="Nominativo del chef" id="chef" placeholder="Nominativo"/><br>
                <label for="place">Luogo</label>
                <input type="text" name="Luogo" id="place" placeholder="Luogo"/><br>
                <label for="price">Prezzo</label>
                <input type="number" min="0" oninput="validity.valid||(value='');" name="Prezzo" id="price" placeholder="Prezzo"/><br>
                <label for="totTickets">Posti disponibili</label>
                <input type="number" min="1" oninput="validity.valid||(value='');" name="Posti disponibili" id="totTickets" placeholder="Numero posti"/><br>
                <label for="courseDescription">Breve descrizione del corso</label><br>
                <textarea name="Breve descrizione del corso" id="courseDescription" placeholder="Descrizione" rows="5" cols="40"></textarea><br>
            </div>
            
            <div class="tab">
                <h2>Compila i seguenti campi</h2>
                <label for="date">Data di inizio delle lezioni</label>
                <input type="date" name="Data di inizio delle lezioni" id="date"/><br>
                <label for="numLessons">Numero di lezioni</label>
                <input type="number" min="1" max="15" oninput="validity.valid||(value='');" name="Numero di lezioni" id="numLessons" placeholder="1"/><br>
                <h3>Orario delle lezioni</h3>
                <label for="startTime">Orario di inizio</label><br>
                <input type="time" name="Orario di inizio delle lezioni" id="startTime"><br>
                <label for="endTime">Orario di fine</label><br>
                <input type="time" name="Orario di fini delle lezioni" id="endTime"/><br>
            </div>
            
            <div class="tab">
                <h2>Compila i seguenti campi</h2>
            </div>

            <div class="tab">
                <h2>Riepilogo</h2>
                <p>Nome del corso: </p>
                <p>Immagine del corso: </p>
                <div id="fileDisplayArea"></div>
                <p>Categoria: </p>
                <p>Nominativo del chef: </p>
                <p>Luogo: </p>
                <p>Prezzo: </p>
                <p>Posti disponibili: </p>
                <p>Breve descrizione del corso: </p><br>
                <br>
                <br><p>Data di inizio delle lezioni: </p>
                <p>Numero di lezioni: </p>
                <p>Orario di inizio delle lezioni: </p>
                <p>Orario di fine delle lezioni: </p><br>
                <br>
                <div class="lessons"></div>
            </div>
            
                <div>
                    <div id="buttons-container">
                        <input type="submit" id="prevBtn" value="Indietro">
                        <input type="submit" id="nextBtn" value="Avanti">
                    </div>
                </div>
            
                <div style="text-align:center;margin-top:40px;">
                    <span class="step"></span>
                    <span class="step"></span>
                    <span class="step"></span>
                    <span class="step"></span>
                </div>
            
        </form>
    </section>
</div>