<link rel="stylesheet" type="text/css" href="./site/css/course-page.css"/>
<script src="./site/javascript/buy-tickets.js"></script>
<div id="course-info-container">
  <!-- utility values -->
  <input type="hidden" id="courseId" value="<?php echo $_GET["idevent"]?>">
  <input type="hidden" id="userId" value="<?php echo $templateParams["userId"]?>">
  <input type="hidden" id="price" value="<?php echo $templateParams["info"][0]["price"]?>">
  <input type="hidden" id="ticketsLeft" value="<?php echo $templateParams["ticketsLeft"]?>">
  <!-- -->
	<aside id="buy-ticket">
		<button id="get-course">PARTECIPA AL CORSO</button>
		<div><div id="arrow"></div></div>
		<section class="hide">
    <!-- <input type="number" name="quantity" id="quantity" value="1"/> -->
      <div class="item-quantity course-page">
        <label>Quantità: </label>
        <button class="sub btn-transparent">-</button>
        <span class="num">1</span>
        <button class="add btn-transparent">+</button>
      </div>
			<p>Posti disponibili:
				<?php
					echo $templateParams["ticketsLeft"];
				?>
			</p>
			<p>Orario:
				<?php
					echo $templateParams["time"][0]["startTime"]." - ".$templateParams["time"][0]["endTime"];
				?>
			</p>
			<p>Totale: <span class="total-placeholder"></span>€</p>
			<button id="buy">AGGIUNGI AL CARRELLO</button>
		</section>
	</aside>
</div>
	<section id="course-page-info">
		<nav id="navigate-course">
			<ul>
				<li><a href="index.php">Home</a></li>
			</ul>
			<ul>
				<li><a href="course.php">Corsi</a></li>
			</ul>
		</nav>
		<p id="course-name">
			<?php
				echo $templateParams["info"][0]["title"];
			?>
		</p>
		<section id="section-info">
			<div class="details">
    			<img class="icon" src="./site/img/icons/category.png" alt="">
	    		<p class="info">TIPOLOGIA</p>
				<p id="type-of-course">
					<?php
						echo $templateParams["info"][0]["category"];
					?>
				</p>
			</div>
			<div class="details">
				<img class="icon" src="./site/img/icons/place.png" alt="">
				<p class="info">LUOGO</p>
				<p id="place">
					<?php
						echo $templateParams["info"][0]["place"];
					?>
				</p>
			</div>
			<div class="details">
				<img class="icon" src="./site/img/icons/time.png" alt="">
				<p class="info">DURATA</p>
				<p id="time">
					<?php
						echo $templateParams["info"][0]["numLessons"]." lezione da ".$templateParams["info"][0]["duration"]." ore";
					?></p>
			</div>
			<div class="details">
				<img class="icon" src="./site/img/icons/price.png" alt="">
				<p class="info">PREZZO CORSO</p>
				<p id="priceInfo">
					<?php
						echo "€".$templateParams["info"][0]["price"];
					?></p>
			</div>
		</section>
	</section>
	<section id="course-page-lessons">
		<p id="course-description">
			<?php
				echo $templateParams["info"][0]["description"];
			?>
		</p>
		<p id="chef">
			<?php
				echo "Corso preparato da ".$templateParams["info"][0]["chef"].".";
			?>
		</p>
		<?php foreach($templateParams["lessons"] as $lesson): ?>
			<p class="title">
				<?php
					echo $lesson["title"];
				?>
			</p>
      <p>
				<?php
					echo $lesson["date"];
				?>
			</p>
			<p class="description">
				<?php
					echo $lesson["description"];
				?>
			</p>
		<?php endforeach; ?>	
	</section>