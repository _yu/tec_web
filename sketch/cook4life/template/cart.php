<header id="cart-banner">
  <h2>Carrello</h2>
  <script src="./site/javascript/cart.js"></script>
</header>

<?php if($templateParams["cart-item"] == "notLoggedIn"): ?>
<div id="cart-not-logged-in"> 
  <button type="submit" onclick="window.location.href = './login.php';">clicca qui per accedere al tuo account</button>
</div>
<?php else: ?>

<section id="cart-items">
  <?php foreach($templateParams["cart-item"] as $item): ?>
  <div class="item">
      <!-- utility values -->
      <input type="hidden" id="itemId" value=<?php echo $item["id"]?>>
      <input type="hidden" id="price" value=<?php echo $item["price"]?>>
      <input type="hidden" id="ticketsLeft" value=<?php echo $item["ticketsLeft"]?>>
      <!-- -------------- -->
      <img src=<?php echo "./site/img/courses/".$item["image"]?> alt="cart item">
      <div class="item-info">
        <p class="item-name"> <?php echo $item["title"]?> <br> <?php echo $item["chef"]?> </p>
        <p class="item-date"><?php echo $item["date"]?></p>
      </div>
      <div class="item-quantity">
        <button class="sub btn-transparent">-</button>
        <span class="num"><?php echo $item["quantity"]?></span>
        <button class="add btn-transparent">+</button>
      </div>
      <p class="item-total">Totale: <span class="total-placeholder"></span>€</p>
      <div class="item-remove">
        <button class="btn-transparent">Rimuovi</button>
      </div>
  </div>
  <div class="border"></div>
  <?php endforeach; ?>
</section>

<section id="cart-total">
  <p>Il totale del tuo carrello è xxx.xx$</p>
  <button class="btn-transparent" type="submit" onclick="window.location.href = './payment.php';">Procedi al pagamento</button>
</section>

<?php endif; ?>