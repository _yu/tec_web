<link rel="stylesheet" type="text/css" href="./site/css/account.css"/>
<script src="./site/javascript/sha512.js"></script>
<script src="./site/javascript/formUtility.js"></script>
<script src="./site/javascript/modify-account.js"></script>
<script src="./site/javascript/fixFooter.js"></script>
<div class="container">
<a class="go-back" href="account.php">Torna al mio account</a><br>
<section id="your-account">
    <h2>Il tuo account</h2><br>
    <h3>Nome</h3>
    <p id="nameInfo"><?php echo $templateParams["info"]["username"];?></p>
    <button class="edit-info" id="nameBtn">Modifica</button><br><br>
    <h3>E-mail</h3>
    <p id="emailInfo"><?php echo $templateParams["info"]["email"];?><br></p>
    <button class="edit-info" id="emailBtn">Modifica</button><br><br>

    <h3>Password</h3>
    <p>********</p>
    <button class="edit-info" id="passwordBtn">Modifica</button><br> 
    <a href="account.php">Indietro</a><br>    
      
    <!-- The Modal -->
    <div id="nameModal" class="modal">

    <!-- Modal content -->
        <div class="modal-content">
            <form action=# method="post">
                <label for="name">Nome</label><br>
                <input type="text" name="name" id="name"><br>
                <span id="name_error" class="error"></span><br>
                <input type="submit" value="Salva" id="nameSubmit">
            </form>
        </div>
    </div>

    <div id="emailModal" class="modal">

    <!-- Modal content -->
        <div class="modal-content">
            <form action=# method="post">
                <label for="email">E-mail</label><br>
                <input type="text" name="email" id="email"><br>
                <span id="email_error" class="error"></span><br>
                <input type="submit" value="Salva" id="emailSubmit">
            </form>
        </div>
    </div>

    <div id="passwordModal" class="modal">

    <!-- Modal content -->
        <div class="modal-content">
            <form action=# method="post" id="changePwd">
                <label for="password">Password</label><br>
                <input type="password" name="password" id="password" autocomplete="off"><br>
                <p>Usa almeno 8 caratteri</p><br>
                <label for="v-password">Verifica password</label><br>
                <input type="password" name="v-password" id="v-password" autocomplete="off"><br>
                <span id="password_error" class="error"></span><br>
                <input type="submit" value="Salva" id="passwordSubmit">
            </form>
        </div>
    </div>
</section>
</div>