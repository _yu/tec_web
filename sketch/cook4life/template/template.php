<!--
    This is the template. It acts as the base for all the website pages since all pages have
    nav and footer in common.
-->

<!DOCTYPE html>
<html lang="it">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php echo $templateParams["title"]; ?></title>
    <?php
    /* for eventual js scripts*/
    if(isset($templateParams["js"])):
        foreach($templateParams["js"] as $script):
    ?>
        <script src="<?php echo $script; ?>"></script>
    <?php
        endforeach;
    endif;
    ?>
    <link rel="stylesheet" type="text/css" href="./site/css/style.css" />
    <script src="./site/javascript/jquery-3.4.1.min.js"></script>
    <script src="./site/javascript/menu.js"></script>
    <script src="./site/javascript/banner.js"></script>
</head>
<body>
  <nav id="header-nav">
	<div class="nav-bar-container">
		<ul>
			<li id="sitename">
				<a href="index.php" ><img src="./site/img/icons/logo.png" alt="homepage" /></a>
			</li>
      <li id="menu"><button id="menu-toggle-btn" class="menu-icon-off btn-transparent"></button></li>
      <li id="separator"></li>
      <li id="notif"><button id="notif-toggle-btn" class="notif-icon-off btn-transparent"></button></li>
			<li id="cart"><a href="cart.php" ><img src="./site/img/icons/cart.png" alt="cart" /></a></li>
		</ul>
	</div>
	<div id="toggle-menu-container">
		<ul>
      <?php if (!isUserLoggedIn()): ?>
              <?php echo "<li><a href=\"login.php\">Accedi</a></li>";?>
      <?php endif; ?>
      <?php if (isUserLoggedIn()): ?>
          <?php echo "<li><a href=\"account.php\">Il mio account</a></li>";?>
      <?php endif; ?>
			<li><a href="course.php">Tutti i corsi</a></li>
			<li><a href="course.php?category=pastery">Pasticceria</a></li>
			<li><a href="course.php?category=italian-cuisine">Cucina italiana</a></li>
			<li><a href="course.php?category=international-cuisine">Cucina internazionale</a></li>
			<li><a href="course.php?category=tasting">Degustazione</a></li>
            <li>
                <?php if (isUserLoggedIn()): ?>
                    <?php echo "<a href=\"logout.php\" >Esci</a>";?>
	            <?php endif; ?>
            </li>
		</ul>
	</div>
  <div id="toggle-notif-container">
  <!-- utility values -->
  <input type="hidden" id="notifYN" value=<?php if($templateParams["notifications"] == "none") {echo "N";} else {echo "Y";} ?>>
  <!-- -------------- -->
		<ul>
    <?php if($templateParams["notifications"] == "none"): ?>
    <li><a href="#notif-none"> Non ci sono notifiche </a> </li>
    <?php else: ?>
      <?php if(isUserLoggedIn()): ?>
        <?php foreach($templateParams["notifications"] as $notification): ?>
          <?php if($notification["type"] == "sold-out"): ?>
			    <li class="notif"><input type="hidden" value=<?php echo $notification["id"]?>><a href="course-details.php?idevent=<?php echo $notification["idCourse"]?>&idNotif=<?php echo $notification["id"]?>">Congratulazioni! Il tuo corso "<?php echo $notification["titleCourse"] ?>" è sold-out!</a></li>
          <?php elseif($notification["type"] == "deleted"): ?>
			    <li class="notif"><input type="hidden" value=<?php echo $notification["id"]?>><a href="#deleted">Attenzione! Il corso "<?php echo $notification["titleCourse"] ?>" è stato annullato!</a></li>
          <?php endif; ?>
        <?php endforeach; ?>
      <?php endif; ?>
    <?php endif; ?>
		</ul>
	</div>
  </nav>
  
  <main>
  <?php
  if(isset($templateParams["filename"])){
      require($templateParams["filename"]);
  }
  ?>
  </main>
            
  <footer>
      <div class="socials">
          <p>Connect with Cook 4 Life</p>
          <a href="#facebook"><img src="./site/img/icons/facebook.png" alt="facebook" class="footer-icon" /></a>
          <a href="#twitter"><img src="./site/img/icons/twitter.png" alt="twitter" class="footer-icon" /></a>
          <a href="#instagram"><img src="./site/img/icons/instagram.png" alt="instagram" class="footer-icon" /></a>
      </div>
      <div class="footer-links">
          <ul>
              <li><a href="#privacy">Informativa sulla privacy</a></li>
              <li><a href="#tos">Condizioni di utilizzo</a></li>
              <li><a href="#contacts">Contatti</a></li>
          </ul>
      </div>
      <p>Copyright &copy; 2020</p>
  </footer>
</body>
</html>