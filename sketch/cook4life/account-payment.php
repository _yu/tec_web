<?php

require_once 'bootstrap.php';
require_once 'database/dbLogin.php';
require_once 'database/dbAccount.php';
require_once 'database/dbPayment.php';

if (isUserLoggedIn()){
    $templateParams["title"] = "C4L - Your payment";

    $templateParams["filename"] = "account-payment.php";
    if(isset($_POST["owner"], $_POST["card"], $_POST["month"], $_POST["year"])) { 
        if(insertNewCard($dbh->getDb(), $_POST["owner"], $_POST["card"], $_POST["month"], $_POST["year"]) == false) {
            $templateParams["add_error"] = "La carta che hai inserito è già associato a un account. Controlla e riprova";
        } else {
            redirect("./account-payment.php");
        }
    }
    if(isset($_SESSION['user_id'])){
        $templateParams["card"] = getPaymentNormal($dbh->getDb());
    }
}


if (isset($_POST["delete"])) {
    deleteCard($dbh->getDb(), $_POST["delete"]);
}

require 'template/template.php';

?>