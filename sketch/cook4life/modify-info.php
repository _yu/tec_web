<?php

require_once 'bootstrap.php';
require_once 'database/dbLogin.php';
require_once 'database/dbAccount.php';

if (isUserLoggedIn()){
    $templateParams["title"] = "C4L - MyAccount";
    $templateParams["filename"] = "account-info.php";
    $templateParams["info"] = getInfo($dbh->getDb());
}

require 'template/template.php';

?>