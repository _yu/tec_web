<?php
require_once 'bootstrap.php';
require_once 'database/dbHome.php';
require_once 'database/dbNotification.php';
require_once 'database/dbAccount.php';
require_once 'database/dbLogin.php';

$templateParams["title"] = "C4L - Home";
$templateParams["filename"] = "home.php";
$templateParams["banners"] = getBanner($dbh->getDb());
$templateParams["categories"] = getCategory($dbh->getDb());

define("BANNER_DIR", "./site/img/banner/");
define("CATEGORY_DIR", "./site/img/categories/");

require 'template/template.php';
?>