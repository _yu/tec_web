<?php
define("UPLOAD_DIR", "./site/img/");
require_once("utils/functions.php");
require_once("database/dbConnection.php");
$dbh = new DatabaseHelper("localhost", "root", "", "blogtw");
$dbh->sec_session_start();
require_once 'utils/notification-check.php';

if(isset($_POST["idNotification"])){
  deleteNotification($dbh->getDb(), $_POST["idNotification"]);
}

?>