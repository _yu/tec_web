<?php

require_once 'bootstrap.php';
require_once 'database/dbLogin.php';
require_once 'database/dbAccount.php';
require_once 'database/dbAdmin.php';

if (isUserLoggedIn()){
    $templateParams["title"] = "C4L - MyAccount";

    $dbResult = getAccountType($dbh->getDb());
    if ($dbResult == "normal") {
        $templateParams["filename"] = "account-normal.php";
        $templateParams["summary"] = getInfo($dbh->getDb());
        $templateParams["currentCourses"] = getCurrentCoursesNormal($dbh->getDb());
    } else if ($dbResult == "organiser") {
        $templateParams["filename"] = "account-organiser.php";
        $templateParams["summary"] = getInfo($dbh->getDb());
    } else if($dbResult == "admin") {
      $templateParams["filename"] = "account-admin.php";
      $templateParams["summary"] = getInfo($dbh->getDb());
      $templateParams["courses"] = getAllCourses($dbh->getDb());
      $templateParams["normals"] = getAllNormalUsers($dbh->getDb());
      $templateParams["organisers"] = getAllOrganisers($dbh->getDb());
    }
}

if(isset($_POST["remove_courseId"])){
  removeCourse($dbh->getDb(), $_POST["remove_courseId"]);
}

if(isset($_POST["remove_userId"])){
  removeUser($dbh->getDb(), $_POST["remove_userId"]);
}
require 'template/template.php';

?>