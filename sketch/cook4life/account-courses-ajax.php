<?php

require_once 'bootstrap.php';
require_once 'database/dbLogin.php';
require_once 'database/dbAccount.php';

/*NORMAL*/

$output = "";
if (isset($_POST["none"])) {
    $templateParams["courses"] = getBoughtCoursesNormal($dbh->getDb());
    if (!empty($templateParams["courses"])) {
        foreach($templateParams["courses"] as $course) {
            $output .= "<a href=\"course-details.php?idevent=".$course["id"]."\">".$course["title"]."</a><br>";
        }
    } else {
        $output = "Non sei iscritto a nessun corso! Clicca <a href=\"course.php\">qui</a> per trovarne uno!<br>";
    }
    echo $output;
}

$output = "";
if (isset($_POST["getNew"])) {
    $templateParams["courses"] = getCurrentCoursesNormal($dbh->getDb());
    if (!empty($templateParams["courses"])) {
        foreach($templateParams["courses"] as $course) {
            $output .= "<a href=\"course-details.php?idevent=".$course["id"]."\">".$course["title"]."</a><br>";
        }
    } else {
        $output = "Non sei iscritto a nessun corso! Clicca <a href=\"course.php\">qui</a> per trovarne uno!<br>";
    }
    echo $output;
}

$output = "";
if (isset($_POST["getOld"])) {
    $templateParams["courses"] = getOldCoursesNormal($dbh->getDb());
    if (!empty($templateParams["courses"])) {
        foreach($templateParams["courses"] as $course) {
            $output .= "<a href=\"course-details.php?idevent=".$course["id"]."\">".$course["title"]."</a><br>";
        }
    } else {
        $output = "Non hai corsi passati!<br>";
    }
    echo $output;
}

/*ORGANISER*/

$output = "";
if (isset($_POST["noneO"])) {
    $templateParams["courses"] = getAllCoursesOrganiser($dbh->getDb());
    if (!empty($templateParams["courses"])) {
        foreach($templateParams["courses"] as $course) {
            $output .= "<div class=\"current-courses-container\">
            <a href=\"course-details.php?idevent=".$course["id"]."\">".$course["title"]."</a>
            </div>";
        }
    } else {
        $output = "Non hai ancora organizzato nessun corso! Clicca <a href=\"coursecreate-course.php\">qui</a> per organizzarne uno!<br>";
    }
    echo $output;
}

$output = "";
if (isset($_POST["getNewO"])) {
    $templateParams["courses"] = getCurrentCoursesOrganiser($dbh->getDb());
    if (!empty($templateParams["courses"])) {
        foreach($templateParams["courses"] as $course) {
            $output .= "<div class=\"current-courses-container\" id=\"container".$course["id"]."\" >
                        <a href=\"course-details.php?idevent=".$course["id"]."\">".$course["title"]."</a>
                        <button id=\"".$course["id"]."\" class=\"delete-course\">Rimuovi</button>
                        </div>";
        }
    } else {
        $output = "Non hai ancora organizzato nessun corso! Clicca <a href=\"coursecreate-course.php\">qui</a> per organizzarne uno!<br>";
    }
    echo $output;
}

$output = "";
if (isset($_POST["getOldO"])) {
    $templateParams["courses"] = getOldCoursesOrganiser($dbh->getDb());
    if (!empty($templateParams["courses"])) {
        foreach($templateParams["courses"] as $course) {
            $output .= "<a href=\"course-details.php?idevent=".$course["id"]."\">".$course["title"]."</a><br>";
        }
    } else {
        $output = "Non hai corsi passati!";
    }
    echo $output;
}

?>