<?php

require_once 'bootstrap.php';
require_once 'database/dbLogin.php';
require_once 'database/dbAccount.php';

if (isset($_POST["newName"])) {
    setNewName($dbh->getDb(), $_POST["newName"]);
    echo $_POST["newName"];
}

if (isset($_POST["newEmail"])) {
    if (setNewEmail($dbh->getDb(), $_POST["newEmail"]) == true) {
        echo $_POST["newEmail"];
    } else {
        echo "fail";
    }
}

if (isset($_POST["newPassword"])) {
    setNewPassword($dbh->getDb(), $_POST["newPassword"]);
    echo "success";
}

?>