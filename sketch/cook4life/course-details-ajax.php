<?php

require_once 'bootstrap.php';
require_once 'database/dbCourse.php';
require_once 'database/dbNotification.php';

/* update notifications */
if(isset($_SESSION['user_id'])){
  $userType = getAccountType($dbh->getDb());
  if($userType == "organiser"){ 
    if(isset($_POST['addToCart_courseId'], $_POST['addToCart_quantity'])){
      echo "fail";
    }
  } else if($userType == "normal"){      
    if(isset($_POST['addToCart_courseId'], $_POST['addToCart_quantity'])){
      if (addToCart($dbh->getDb(), $_SESSION['user_id'],
        $_POST['addToCart_courseId'], $_POST['addToCart_quantity']) == true) {
          echo "done";
      } else {
        echo "ended";
      }
  }
}
}

?>