<?php

require_once 'bootstrap.php';
require_once 'database/dbLogin.php';
require_once 'database/dbCourseCreation.php';
define("COURSES_DIR", "./site/img/courses/");
define("INPUTS", 11);

$courseArr = array();
$lessonArr = array();

if (isUserLoggedIn()){
    $templateParams["title"] = "C4L - Create course: general information";
    $templateParams["filename"] = "create-course-form.php";

    if ($_POST) {
        $i = 0;
        $index = 1;
        foreach($_POST as $post) {
            array_push($courseArr, $post);
            $i++;
            if ($i == INPUTS) {
                break;
            }
        }

        $i = 0;
        $lesson = array(
            "title" => "",
            "description" => ""
        );
        foreach($_POST as $post) {
            if ($i >= INPUTS) {
                if ($i%2!=0) {
                    $lesson["title"] = $post;
                } else {
                    $lesson["description"] = $post;
                    array_push($lessonArr, $lesson);
                }
            }
            $i++;
        }
        array_push($courseArr, $_SESSION['user_id']);
        if (addCourse($courseArr, $lessonArr, $_FILES["image"], $dbh->getDb()) == SUCCESS) {
            unset($_POST);
            redirect("./account.php");
        }   
    }
}
require 'template/template.php';

?>