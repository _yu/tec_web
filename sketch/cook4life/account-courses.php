<?php

require_once 'bootstrap.php';
require_once 'database/dbLogin.php';
require_once 'database/dbAccount.php';
require_once 'database/dbCourse.php';

if (isUserLoggedIn()){
    $templateParams["title"] = "C4L - Your courses";

    $dbResult = getAccountType($dbh->getDb());
    if ($dbResult == "normal") {
        $templateParams["filename"] = "account-normal-courses.php";
    } else if ($dbResult == "organiser") {
        $templateParams["filename"] = "account-organiser-courses.php";
    }
}

if (isset($_POST["delete-course"])) {
    removeCourse($dbh->getDb(), $_POST["delete-course"]);
}

require 'template/template.php';

?>