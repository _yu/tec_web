<?php

require_once 'bootstrap.php';
require_once 'database/dbCourse.php';
require_once 'database/dbNotification.php';

$templateParams["title"] = "C4L - Course";
$templateParams["filename"] = "course-page.php";

if (isset($_GET['idevent'])) {
    $idEvent = $_GET['idevent'];
}

if (isset($_GET['idNotif'])) {
  deleteNotification($dbh->getDb(), $_GET['idNotif']);
}

if(isset($_SESSION['user_id'])){
  $templateParams["userId"] = $_SESSION['user_id'];
} else {
  $templateParams["userId"] = "notLoggedIn";
}


/* update notifications */
if(isset($_SESSION['user_id'])){
  $userType = getAccountType($dbh->getDb());
  if($userType == "organiser"){ 
    $templateParams["notifications"] = getOrganiserNotifs($dbh->getDb(), $_SESSION['user_id']);
    
  } else if($userType == "normal"){      
    $templateParams["notifications"] = getNormalNotifs($dbh->getDb(), $_SESSION['user_id']);
    
  }
  if(empty($templateParams["notifications"])){      
    $templateParams["notifications"] = "none";
  }
} else {      
  $templateParams["notifications"] = "none";
}

$templateParams["ticketsLeft"] = getTicketsLeft($idEvent, $dbh->getDb());
$templateParams["time"] = getTime($idEvent, $dbh->getDb());
$templateParams["info"] = getGeneralInfo($idEvent, $dbh->getDb());
$templateParams["lessons"] = getLessons($idEvent, $dbh->getDb());

require 'template/template.php';

?>