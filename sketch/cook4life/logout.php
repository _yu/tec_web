<?php
    require_once 'bootstrap.php';
    $_SESSION = array(); //delete all session's variables
    $params = session_get_cookie_params(); //get cookie variables
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"], $params["secure"], $params["httponly"]); //delete current cookie variables
    session_destroy(); //delete session

    redirect("./index.php");
    require 'template/template.php';
?>