<?php

require_once 'bootstrap.php';
require_once 'database/dbLogin.php';
require_once 'database/dbRegistration.php';

if(isset($_POST["name"], $_POST["email"], $_POST["p"], $_POST["type"])) { 
    if(insertNewUser($dbh->getDb(),  $_POST["name"], $_POST["email"], $_POST["p"], $_POST["type"]) == false) {
        $templateParams["signUp_error"] = "L'email che hai inserito è già associato a un account. Controlla e riprova";
    } else {
        login($_POST["email"], $_POST["p"], $dbh->getDb());
        redirect("./index.php");
    }
}

$templateParams["title"] = "C4L - Sign Up";
$templateParams["filename"] = "registration-form.php";

require 'template/template.php';
?>