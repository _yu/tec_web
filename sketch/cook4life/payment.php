<?php
require_once 'bootstrap.php';
require_once 'database/dbCart.php';

$templateParams["title"] = "C4L - Payment";
$templateParams["filename"] = "payment.php";

if(isset($_SESSION['user_id'])){
  $templateParams["idUser"] = isset($_SESSION['user_id']);
  if (empty(getCard($dbh->getDb()))) {
    $templateParams["cards"] = "noCards";
  } else {
    $templateParams["cards"] = getCard($dbh->getDb());
  }
} else {
  $templateParams["cart-item"] = "notLoggedIn";
}

if(isset($_POST["processPayment"])){
  if($_POST["processPayment"]=="true"){
    processPayment($dbh->getDb(), $_SESSION['user_id']);
  }
}

require 'template/template.php';
?>