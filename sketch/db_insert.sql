-- -------------*ACCOUNT+LOGIN ATTEMPS*---------------  
INSERT INTO `user`
VALUES
(1, '_yu', 'yuqi@mail.com', '0cab6d3f19c62ecf7049f447bc8c2b90fb3639b551f384e904f8c86f94779c6b1974198a17fd7ad446e7ea7a306fb4b9381b77efe94d1952ad365d77632ecae1', '2896f78c05c5b679c52bcd88d44d468606b91b37f29c53e6cdebf2e57bb9d161707abf660975729c356e4581789d0da59289443bd791b0641b419d1fb846a463', 'admin'),
(2, 'user1', 'user1@mail.com', 'ca54df4861e66e66c1798c257a0e59a5fbcc9261c0efbfad7c4de347df8fd21269765b285604011d40893e29d2103953b9b46bf8e2fb03e4e160d66410b96d0c', '18446ce842213b8c9138606f91f9ebf49d13ddf9260df1072dd907e5f3902432600c869e4a010c606d6db6dab85b42dd224cefbc2c2e4391cacbb5f5cde7503f', 'normal'),
(3, 'organiser1', 'organiser1@mail.com', 'ca54df4861e66e66c1798c257a0e59a5fbcc9261c0efbfad7c4de347df8fd21269765b285604011d40893e29d2103953b9b46bf8e2fb03e4e160d66410b96d0c', '18446ce842213b8c9138606f91f9ebf49d13ddf9260df1072dd907e5f3902432600c869e4a010c606d6db6dab85b42dd224cefbc2c2e4391cacbb5f5cde7503f', 'organiser'),
(4, 'organiser2', 'organiser2@mail.com', 'ca54df4861e66e66c1798c257a0e59a5fbcc9261c0efbfad7c4de347df8fd21269765b285604011d40893e29d2103953b9b46bf8e2fb03e4e160d66410b96d0c', '18446ce842213b8c9138606f91f9ebf49d13ddf9260df1072dd907e5f3902432600c869e4a010c606d6db6dab85b42dd224cefbc2c2e4391cacbb5f5cde7503f', 'organiser');


-- -------------*COURSES*---------------         
INSERT INTO `course`
        (`id`, `title`, `image`, `idCategory`, `chef`, `place`, `price`, `totTickets`, `ticketsLeft`,
        `description`, `numLessons`, `date`, `startTime`, `endTime`, `idOrganiser`)
VALUES 
(1, "Basi della cucina", "basiCARD.jpg", 2, "Davide Legno", "Milano", 490, 50, 50, "Il corso Basi della Cucina, garanzia di tradizione e autenticità, è articolato in sette incontri: un percorso completo per padroneggiare i fondamenti e le tecniche della cucina italiana", 7, "2020-01-22", "19:00:00", "22:00:00", 3),
(2, "Pizze, focacce e torte salate", "pizze-focacce-e-torte-salate-copertina.jpg", 2, "Riccardo Lucchi", "Bologna", 220, 50, 50, "Pochi ingredienti, tante ricette: il meglio della nostra tradizione culinaria in tre lezioni.", 3, "2020-01-27", "09:30:00", "12:30:00", 3),
(3, "Ravioli al vapore", "ravioli.png", 3, "Yuqi Sun", "Milano", 60, 50, 0, "this is a description", 2, "2020-01-02", "12:30:00", "16:30:00", 4),
(4, "Piadina", "piadina.png", 2, "Elena Rughi", "Forli", 20, 25, 25, "this is a description", 1, "2020-02-04", "13:30:00", "16:30:00", 4),
(5, "Lasagna", "lasagna.png", 2, "Giorgia Rondinini", "Forli", 100, 50, 50, "this is a description", 3, "2020-01-28", "08:00:00", "11:00:00", 4),
(6, "Dolci al cucchiaio", "cucchiaioCARD.jpg", 1, "Cecilia Teodorani", "Ravenna", 90, 40, 40, "this is a description", 3, "2020-02-10", "19:30:00", "21:00:00", 4),
(7, "Le carni", "carniCARD.jpg", 2, "Matteo Castellucci", "Forli", 220, 30, 30, "Un corso completo, strutturato in tre lezioni. Manzo, maiale e animali da cortile sono utilizzati in ricette, da cui prendere spunto per arricchire le tecniche e le conoscenze in cucina. Sono presenti ricette regionali, tecniche di servizio e utilizzi innovativi di tagli: un corso tutto da scoprire!", 3, "2020-01-25", "09:30:00", "12:00:00", 3),
(8, "Insalate dal mondo", "insalata.jpg", 3, "Matteo Castellucci", "Imola", 150, 50, 50, "Le più buone insatale da tutto il mondo", 3, "2020-02-25", "09:30:00", "12:00:00", 3),
(9, "Dolci della luna", "moon-cake.jpg", 3, "Marco Hu", "Faenza", 300, 25, 25, "Impara a realizzare la torta lunare! Dolce cinese tradizionalmente consumato durante la Festa di metà autunno, una delle festività cinesi più importanti, il famoso chef Marco Hu ci regala la sua presenza per due lezioni ricche e intensivie per imparare tutti i segreti della torta lunare", 2, "2020-02-19", "13:00:00", "17:00:00", 3),
(10, "Baguette e altri pani", "baguettes.jpg", 3, "Giada Amaducci", "Ravenna", 150, 35, 35, "Descrizione del corso", 4, "2020-03-01", "08:30:00", "12:30:00", 3),
(11, "Gorgonzola a mai più", "gorgonzola.jpg", 2, "Giammarco Amadori", "Roma", 250, 40, 40, "Descrizione del corso", 3, "2020-01-28", "14:30:00", "17:00:00", 3),
(12, "Tangyuan, dolci dalla cina", "tangyuan.jpg", 3, "Yuqi Sun", "Milano", 300, 50, 50, "La rinomata chef Yuqi Sun ci presenta i tangyua, pietanza cinese fatta di farina di riso glutinoso", 1, "2020-02-15", "14:00:00", "17:00:00", 3);


-- -------------*LESSONS*---------------         
INSERT INTO `lesson`
        (`id`, `idCourse`, `title`, `description`, `date`, `startTime`, `endTime`)
VALUES 
(1, 1, "Lezione1", "Descrizione lezione 1", "2020-01-22", "19:00:00", "22:00:00"),
(2, 1, "Lezione2", "Descrizione lezione 2", "2020-01-29", "19:00:00", "22:00:00"),
(3, 1, "Lezione3", "Descrizione lezione 3", "2020-02-05", "19:00:00", "22:00:00"),
(4, 1, "Lezione4", "Descrizione lezione 4", "2020-02-12", "19:00:00", "22:00:00"),
(5, 1, "Lezione5", "Descrizione lezione 5", "2020-02-19", "19:00:00", "22:00:00"),
(6, 1, "Lezione6", "Descrizione lezione 6", "2020-02-26", "19:00:00", "22:00:00"),
(7, 1, "Lezione7", "Descrizione lezione 7", "2020-03-04", "19:00:00", "22:00:00"),

(1, 2, "Lezione1", "Descrizione lezione 1", "2020-01-27", "09:30:00", "12:30:00"),
(2, 2, "Lezione2", "Descrizione lezione 2", "2020-02-03", "09:30:00", "12:30:00"),
(3, 2, "Lezione3", "Descrizione lezione 3", "2020-02-10", "09:30:00", "12:30:00"),

(1, 3, "Lezione1", "Descrizione lezione 1", "2020-01-02", "12:30:00", "16:30:00"),
(2, 3, "Lezione2", "Descrizione lezione 2", "2020-01-09", "12:30:00", "16:30:00"),

(1, 4, "Lezione1", "Descrizione lezione 1", "2020-02-04", "13:30:00", "16:30:00"),

(1, 5, "Lezione1", "Descrizione lezione 1", "2020-01-28", "08:00:00", "11:00:00"),
(2, 5, "Lezione2", "Descrizione lezione 2", "2020-02-04", "08:00:00", "11:00:00"),
(3, 5, "Lezione3", "Descrizione lezione 3", "2020-02-11", "08:00:00", "11:00:00"),

(1, 6, "Lezione1", "Descrizione lezione 1", "2020-02-10", "19:30:00", "21:00:00"),
(2, 6, "Lezione2", "Descrizione lezione 2", "2020-01-29", "19:00:00", "22:00:00"),
(3, 6, "Lezione3", "Descrizione lezione 3", "2020-02-05", "19:00:00", "22:00:00"),

(1, 7, "Lezione1", "Descrizione lezione 1", "2020-01-25", "09:30:00", "12:00:00"),
(2, 7, "Lezione2", "Descrizione lezione 2", "2020-02-01", "09:30:00", "12:00:00"),
(3, 7, "Lezione3", "Descrizione lezione 3", "2020-02-08", "09:30:00", "12:00:00"),

(1, 8, "Lezione1", "Descrizione lezione 1", "2020-02-25", "09:30:00", "12:00:00"),
(2, 8, "Lezione2", "Descrizione lezione 2", "2020-03-03", "09:30:00", "12:00:00"),
(3, 8, "Lezione3", "Descrizione lezione 3", "2020-03-10", "09:30:00", "12:00:00"),

(1, 9, "Lezione1", "Descrizione lezione 1", "2020-02-19", "13:00:00", "17:00:00"),
(2, 9, "Lezione2", "Descrizione lezione 2", "2020-02-19", "13:00:00", "17:00:00"),

(1, 10, "Lezione1", "Descrizione lezione 1", "2020-03-01", "08:30:00", "12:30:00"),
(2, 10, "Lezione2", "Descrizione lezione 2", "2020-03-08", "08:30:00", "12:30:00"),
(3, 10, "Lezione3", "Descrizione lezione 3", "2020-03-15", "08:30:00", "12:30:00"),
(4, 10, "Lezione4", "Descrizione lezione 4", "2020-03-22", "08:30:00", "12:30:00"),

(1, 11, "Lezione1", "Descrizione lezione 1", "2020-01-28", "14:30:00", "17:00:00"),
(2, 11, "Lezione2", "Descrizione lezione 2", "2020-02-04", "14:30:00", "17:00:00"),
(3, 11, "Lezione3", "Descrizione lezione 3", "2020-02-11", "14:30:00", "17:00:00"),

(1, 12, "Lezione1", "Descrizione lezione 1", "2020-02-15", "14:00:00", "17:00:00");





-- -------------*BANNER*---------------  
INSERT INTO `banner` (`id`, `name`, `title`, `img`, `idCourse`)
VALUES 
(1, "salad", "Impara a fare deliziose insalate", "salad.png", 8),
(2, "mooncake", "Hai mai voluto mangiare la luna?", "mooncake.png", 9),
(3, "baguette", "Oh, pane, che delizia! Baguettes ed altro", "baguette.png", 10),
(4, "gorgonzola", "Tutti i segreti del gorgonzola", "gorgonzola.png", 11),
(5, "tangyuan", "Tangyuan: dolci dalla Cina", "tangyuan.png", 12);



-- -------------*CATEGORIES*---------------  
INSERT INTO `category` (`id`, `name`, `title`, `img`)
VALUES 
(1, "pastery", "Corsi di Pasticceria","sweets.png"),
(2, "italian-cuisine", "Corsi di Cucina Italiana", "italian.png"),
(3, "international-cuisine", "Corsi di Cucina Estera", "foreign.png"),
(4, "tasting", "Corsi di Degustazione", "foreign.png");



-- -------------*timeSlots*---------------  
INSERT INTO `timeslot` (`id`, `name`)
VALUES 
(1, "mattina"),
(2, "pomeriggio"),
(3, "sera"),
(4, "weekend");

-- -------------*CART ITEMS*--------------- 
INSERT INTO `cartItem` (`id`, `idUser`, `idCourse`, `quantity`) 
VALUES 
(1, 2, 3, 1), 
(2, 2, 4, 2), 
(3, 2, 5, 1);

-- -----------*NOTIFICATION*--------------------
INSERT INTO `notification` (`id`, `type`, `idUser`, `idCourse`, `titleCourse`)
VALUES
(1, "sold-out", 3, 2, "Pizze, focacce e torte salate");